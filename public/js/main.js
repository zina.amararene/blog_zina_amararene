$(function() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(function () {
        $('#content').trumbowyg({
            lang: 'fr'
        });
        $('#abstract').trumbowyg({
            lang: 'fr'
        });
        $('#contentComment').trumbowyg({
            lang: 'fr'
        });
    });
});
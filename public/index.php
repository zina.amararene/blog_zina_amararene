<?php

require ('../vendor/autoload.php');

use Framework\Router;
use Framework\Request;

$request = new Request();
$router = new Router($request);
$router->run();

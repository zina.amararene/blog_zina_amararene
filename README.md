# Blog professionnel
## Zina Amararene
## Pré-requis
Langage de programmation PHP 7
Programmation oriontée objet POO
Langage de requête SQL pour gérer la base de donnée
Connaitre les principes MVC
Savoir utiliser composer
Le framework Bootstrap pour la partie intégration.

## Description du projet
Le projet consiste à developper mon blog professionnel qui se compose de deux grands groupes de pages :
* Les pages utiles à tous les visiteurs;
* Les pages permettant d'administrer le blog.

![Blog de Zina Amararene](../public/img/blog-zina-home.png)

## Langages utilisés
* PHP, MySQL;
* HTML5, CSS3 de boostrap
* Le projet est développé avec l'architecture MVC.

## Installation du projet sur votre espace personnel
GIT doit être installé.
il faut avoir installé au préalable le package:
* WAMP pour un environnement de développement sous Windows
* MAMP pour un environnement de développement sous Mac
* LAMP pour un environnement de développement sous Linux

Cet environnement est composé:
* Apache: serveur web chargé de délivrer les pages web aux visiteurs
* MySQL: système de gestion de base de données
* PHP:  le programme qui permet au serveur web d'exécuter des pages PHP
* PHPMyAdmin: système de gestion pour MySQL, c'est une interface poue gérer facilement MySQL, voici le lien pour télécharger et installer tout l'ensemble [site de opeclassrooms](https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/4237816-preparez-votre-environnement-de-travail)

## Cloner le dépôt du projet
pour contribuer au projet sur lequel nous n'avons pas les droits ne écriture, apporter les modifications (correction ou améliortaion) dans votre propre copie du projet, il faut copier le dans votre espace personnel puis d'envoyer la contribution en tant que merge request.

Consiste à récuperer tout l'historique et tous les codes source du projet avec GIT. Rendez vous sur la page de mon projet [Zina Amararene](https://gitlab.com/zina.amararene/blog_zina_amararene), vous y voyez la liste des fichiers ainsi qu'un champs contenant l'adresse du dépôt 
Pour cloner le dépôt : cliquez sur le boutton __clone__ et choisir clone with SSH ou HTTPS
Lancer la commande copiée dans le terminal, cela va créer le dossier dans lequel tous les fichiers sources seront téléchargés ainsi que l'historique des modifications. Vous pouvez ouvrir le dossier et le consulter.
Lorsque vous modifiez des fichiers, vous pouvez soumettre vos changements pour validation au propiétaire du projet.

Una autre manière de télécharger le projet est de cliquer sur le bouton de téléchargement.

<?php

namespace Framework;

class View
{
    // nom de fichier associé à la vue
    private $file;
    // titre de la vue défini dans le fichier vue
    private $title;

    // Génère et affiche le contenu
    public function render($template, $data = [])
    {
        // Détermination du nom du fichier vue à partir de $template
        $this->file = '../templates/'.$template.'.php';
        // Génération de la partie spécifique de la vue
        $content  = $this->renderFile($this->file, $data);
        // Génération du gabarit commun utilisant la partie spécifique
        $view = $this->renderFile('../templates/base.php', [
            'title' => $this->title,
            'content' => $content,
        ]);
        // Renvoi de la vue au navigateur
        echo $view;
    }

    // Génère un fichier vue et renvoie le résultat produit
    private function renderFile($file, $data)
    {
        if (file_exists($file)) {
            // Rend les éléments du tableau $data accessibles dans la vue
            extract($data);
            ob_start();
            // Inclut le fichier vue, son résultat est placé dans le tampon de sortie
            require $file;
            return ob_get_clean();
        }
    }

    public function renderEmail($template, $data = [])
    {
        // Détermination du nom du fichier vue à partir de $template
        $this->file = '../templates/'.$template.'.php';
        // Génération de la partie spécifique de la vue
        $content  = $this->renderFile($this->file, $data);
        // Génération du gabarit commun utilisant la partie spécifique
        $view = $this->renderFile('../templates/base_email.php', [
            'title' => $this->title,
            'content' => $content
        ]);
        return $view;
    }
}

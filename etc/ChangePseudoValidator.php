<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class ChangePseudoValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($userInfo)
	{
		if (!$this->token->verifyToken('tokenChangePseudo', $userInfo['tokenChangePseudo'])) {
			$this->errors[] = 'Token non valide';
        }
        
        if (!$this->validator->isValid($userInfo['newPseudo'], 3, 50)) {
            $this->errors[] = 'Le pseudo doit contenir entre 3 et 50 caractères';
	    }

		return count($this->errors) === 0;
	}
}

<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class ContactMeValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($contact)
	{
		if (!$this->token->verifyToken('tokenContactMe', $contact['tokenContactMe'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isValid($contact['name'], 3, 50)) {
			$this->errors[] = 'Le nom doit contenir entre 3 et 50 caractères';
		}

		if (!$this->validator->isValid($contact['firstName'], 3, 50)) {
			$this->errors[] = 'Le prénom doit contenir entre 3 et 50 caractères';
		}

		if (!$this->validator->isValid($contact['subject'], 3, 80)) {
			$this->errors[] = 'Le sujet doit contenir entre 3 et 80 caractères';
		}

		if (!$this->validator->isValidEmail($contact['email'], 10, 50)) {
			$this->errors[] = 'L\'email doit contenir entre 3 et 50 caractères';
		}

		if (!$this->validator->isValid($contact['message'], 10, 2000)) {
			$this->errors[] = 'Le message doit contenir entre 10 et 200 caractères';
		}

		return count($this->errors) === 0;
	}
}

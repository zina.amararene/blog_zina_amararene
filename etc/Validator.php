<?php

namespace Framework;

class Validator
{

	public function clearInput($chain)
	{
		return ($this->isNotEmpty($chain)) ? htmlspecialchars($chain) : false;
	}

	public function isNotEmpty($chain)
	{
		return !empty($chain);
	}

	public function isNull($chain)
	{
		$chain = $this->clearInput($chain);
		return $chain == NULL ? true : false;
	}

	public function isArray($variable)
	{
		return is_array($variable) ? true : false;
	}

	public function isString($chain)
	{
		$content = $this->clearInput($chain);
		return is_string($content);
	}

	public function isNumeric($variable)
	{
		$content = $this->clearInput($variable);
		return is_numeric($content);
	}

	public function checkLength($chain, $lengthMin, $lengthMax)
	{
		$content = $this->clearInput($chain);
		return (strlen($content) >= $lengthMin && strlen($content) <= $lengthMax) ? true : false;
	}

	public function isValid($chain, $lengthMin, $lengthMax)
	{
		return (!$this->isNull($chain) && $this->isString($chain) && $this->checkLength($chain, $lengthMin, $lengthMax)) ? true : false;
	}

	public function isValidEmail($email, $lengthMin, $lengthMax)
	{
		if ($this->isValid($email, $lengthMin, $lengthMax)) {
		    return preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email);
		}
	}

	public function isValidPassword($password, $lengthMin, $lengthMax)
	{
		if ($this->isValid($password, $lengthMin, $lengthMax)) {
		    return preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])#', $password);
	    }
	}

    // valider l'intégrité du formulaire $_POST
	public function validForm(array $form)
	{
        foreach ($form as $key => $value) {

        	if ($this->isNotEmpty($value[0])) {
	        	switch ($value) {
	        		case $this->isArray($value) && preg_match('#@#', $value[0]) :
	        		return $this->isValidEmail($value[0], $value[1], $value[2]);
	        		break;

	        		case $this->isArray($value) :
	        		return $this->isValid($value[0], $value[1], $value[2]);
	        		break;
	        	}
        	}
        }
	}
}

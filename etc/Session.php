<?php

namespace Framework;

class Session
{

	public function getSession()
	{
		return $_SESSION;
	}

	public function setSession($key, $value)
	{
		if (!empty($key) && !empty($value)) {
			$_SESSION[$key] = $value;
		}
	}

	public function unsetSession($key)
	{
		unset($_SESSION[$key]);
	}

	public function destroySession() 
	{
		$_SESSION = [];
		session_destroy();
	}
}

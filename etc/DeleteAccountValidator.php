<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class DeleteAccountValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($account)
	{
		if (!$this->token->verifyToken('tokenDeleteAccount', $account['tokenDeleteAccount'])) {
			$this->errors[] = 'Token non valide';
        }
        
        if (!$this->validator->isValidEmail($account['email'], 8, 50)) {
            $this->errors[] = 'L\'email doit être valide';
	    }

		return count($this->errors) === 0;
	}
}

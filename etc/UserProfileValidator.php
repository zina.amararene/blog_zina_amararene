<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class UserProfileValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($infoUserToEdit)
	{
		if (!$this->token->verifyToken('tokenUserProfile', $infoUserToEdit['tokenUserProfile'])) {
			$this->errors[] = 'Token non valide';
        }
        
        if (!$this->validator->isValid($infoUserToEdit['firstName'], 3, 100)) {
            $this->errors[] = 'Le prénom doit contenir entre 3 et 100 caractères';
	    }

	    if (!$this->validator->isValid($infoUserToEdit['lastName'], 3, 100)) {
            $this->errors[] = 'Le nom doit contenir entre 3 et 100 caractères';
	    }

	    if (!$this->validator->isValidEmail($infoUserToEdit['email'], 8, 50)) {
            $this->errors[] = 'L\'email doit être valide';
	    }

	    if (!$this->validator->isValidEmail($infoUserToEdit['emailConfirmation'], 8, 50)) {
            $this->errors[] = 'L\'email doit être valide';
	    }

	    if (!$infoUserToEdit['email'] == $infoUserToEdit['emailConfirmation']) {
	    	$this->errors[] = 'Veuillez confirmer le même email';
	    }

		return count($this->errors) === 0;
	}
}

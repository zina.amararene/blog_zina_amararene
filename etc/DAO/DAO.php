<?php

namespace Framework\DAO;

abstract class DAO
{
    private $pdo  = null;
    
    public function connectionDBB()
    {
        if ($this->pdo === null || !\is_a($this->pdo, \PDO::class)) {
            return $this->getConnection();
        }

        return $this->pdo;
    }

    private function getConnection()
    {
        $config = require('../config/database.php');

        try {
           $this->pdo = new \PDO($config['dsn'], $config['username'], $config['password']);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $this->pdo;
        } catch (\Exception $errorConnection) {
            sprintf('Erreur de connection : %s', $errorConnection->getMessage());
        }
    }
}

<?php

namespace Framework;

use App\Controllers\HomeController;
use App\Controllers\ArticlesController;
use App\Controllers\ArticleDetailsController ;
use App\Controllers\NewsletterController;
use App\Controllers\ErrorController;
use App\Controllers\MyAccountController;
use App\Controllers\SignInController;
use App\Controllers\ForgotPasswordController;
use App\Controllers\ValidateCommentsController;
use App\Controllers\UserAccountController;
use App\Controllers\UserProfileController;
use App\Controllers\ManageAccountsController;
use  App\Controllers\AddArticleController;
use  App\Controllers\UpdateArticleController;

class Router
{
  private $request;
  private $errorController;
  private $homeController;
  private $articlesController;
  private $articleDetailsController;
  private $newsletterController;
  private $myAccountController;
  private $signInController;
  private $forgotPasswordController;
  private $validateCommentsController;
  private $userAccountController;
  private $userProfileController;
  private $manageAccountsController;
  private $addArticleController;
  private $updateArticleController;

  public function __construct($request)
  {
    $this->request = $request;
    $this->errorController = new ErrorController();
    $this->homeController = new HomeController();
    $this->myAccountController = new MyAccountController();
    $this->articlesController = new ArticlesController();
    $this->articleDetailsController = new ArticleDetailsController();
    $this->newsletterController = new NewsletterController();
    $this->forgotPasswordController = new ForgotPasswordController();
    $this->signInController = new SignInController();
    $this->validateCommentsController = new ValidateCommentsController();
    $this->userAccountController = new UserAccountController();
    $this->userProfileController = new UserProfileController();
    $this->manageAccountsController = new ManageAccountsController();
    $this->addArticleController = new AddArticleController();
    $this->updateArticleController = new UpdateArticleController();
  }

  public function run()
  {
    try {
      $get = $this->request->getGet();
      $post = $this->request->getPost();
      
      if (isset($get['route'])) {
        switch ($get['route']) {
        // Affichage de la page d'accueil
        case 'home' :
          $this->homeController->home($post);
        break;

        // Affichage des articles du blog
        case 'articles' :
          if ($get['action'] == 'posts') {
            $this->articlesController->articles($get['numberPage'] ?? 1);
          }
        break;

        case 'addArticle':
            $this->addArticleController->addArticle($post);
        break;

        case 'updateArticle' :
          if ($get['action'] == 'showArticlesToUpdate') {
            $this->updateArticleController->showArticlesToUpdate();
          }
          if ($get['action'] == 'editArticle') {
            $this->updateArticleController->editArticle($post, $get['idArticle']);
          }
          if ($get['action'] == 'deleteArticle') {
            $this->updateArticleController->deleteArticle($get);
          }
        break;

        // Affichage détail d'un articles et ajouter des commentaires
        case 'articleDetails' :
          $this->articleDetailsController->articleDetails($post, $get['idArticle']);
        break;

        // S'incrire à la newsletter
        case 'newsletter' :
          $this->newsletterController->registerNewsletter($post);
        break;

        // Affichage de la page Mon compte et traitement du formulaire d'inscription
        case 'myAccount' :
          if ($get['action'] == 'register') {
            $this->myAccountController->register($post);
          }
          if ($get['action'] == 'logOut') {
            $this->myAccountController->logOut();
          }
        break; 
        //Modifier le compte user, mot de passe, pseudo et suppression du compte
        case 'userAccount' :
          if ($get['action'] == 'changePassword') {
            $this->userAccountController->changePassword($post);
          }
          if ($get['action'] == 'changePseudo') {
            $this->userAccountController->changePseudo($post);
          }
          if ($get['action'] == 'deleteAccount') {
            $this->userAccountController->deleteAccount($post);
          }
        break;

        // Modifier les informations du profile utilisateur
        case 'userProfile' :
          $this->userProfileController->editProfile($post);
        break;

        // connexion du memebre ou administrateur
        case 'signIn' :
          $this->signInController->signIn($post);
        break;

        case 'administration' :
            $this->signInController->administration();
        break;

        // Mot de passe oublié
        case 'forgotPassword' :
          if ($get['action'] == 'resetPassword') {
            $this->forgotPasswordController->resetPassword($post);
          }
          if ($get['action'] == 'passwordRecovery') {
            $this->forgotPasswordController->verifyTokenResetPassword($get['token']);
          }
          if ($get['action'] == 'editPassword') {
            $this->forgotPasswordController->editPassword($post);           
          }
        break;

        case 'validateComments' :
          if ($get['action'] == 'postComment') {
            $this->validateCommentsController->postComment($get);
          }
          if ($get['action'] == 'removeComment') {
            $this->validateCommentsController->removeComment($get);
          }
          break;

        // Gestion des comptes utilisateurs
        case 'listOfAccounts' :
          if ($get['action'] == 'listOfAccounts') {
            $this->manageAccountsController->listOfAccounts();
          }
          if ($get['action'] == 'createRole') {
            $this->manageAccountsController->createRole($post, $get['userId']);
          }
        break;
        default :
            $this->homeController->home(null);
        }
       // Affichage par défaut de la page d'accueil
      } else {
        $this->homeController->home(null);
      }
    } catch (\Exception $e) {
      $this->errorController->error();
    }
  }
}

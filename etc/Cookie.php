<?php

namespace Framework;

use Framework\Token;

class Cookie
{
	private $token;
	private $cookie;

	public function __construct()
	{
		$this->token = new Token();
        $this->cookie = $this->getCookie();
	}

	public function getCookie()
    {
        return $_COOKIE;
	}
	
	public function setCookie(array $cookie, $cookieName, $expirationTime)
	{
		if (!empty($cookie)) {
		$token = $this->token->generateToken('tokenCookie');
		$cookie[] = array('token' => $token);
        setcookie($cookieName, json_encode($cookie), $expirationTime, '/', null, false, true);
		}
	}

	public function destroyCookie($cookieName)
	{
        setcookie($cookieName, NULL, -1, '/');
	}

	public function verifyCookie($cookieName, $password, $pseudo)
	{
		if (isset($this->cookie)) {
			$cooki = $this->cookie;
            $cookie = json_decode($cooki[$cookieName]);
			$is_password_correct = password_verify($password, $cookie['password']);
			return $pseudo == $cookie['pseudo'] && $is_password_correct ? true : false;
		}
	}
}

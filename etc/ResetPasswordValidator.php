<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class ResetPasswordValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrorsResetPassword()
	{
		return $this->errors;
	}

	public function validate($user)
	{
		if (!$this->token->verifyToken('tokenForgotPassword', $user['tokenForgotPassword'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isValidEmail($user['email'], 8, 50)) {
			$this->errors[] = 'L\'email doit être valide';
		}

		return count($this->errors) === 0;
	}
}

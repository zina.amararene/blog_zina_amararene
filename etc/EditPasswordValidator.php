<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class EditPasswordValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrorsEditPassword()
	{
		return $this->errors;
	}

	public function validate($resetPassword)
	{
		if (!$this->token->verifyToken('tokenUpdatePassword', $resetPassword['tokenUpdatePassword'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isValidPassword($resetPassword['newPassword'], 8, 70)) {
            $this->errors[] = 'Le mot de passe doit contenir entre 8 et 70 caractères, être composé de minuscules, majuscules et de chiffres';
		}

		if (!$this->validator->isValidPassword($resetPassword['confirmNewPassword'], 8, 70)) {
            $this->errors[] = 'Le mot de passe doit contenir entre 8 et 70 caractères, être composé de minuscules, majuscules et de chiffres';
		}

		if (!$resetPassword['newPassword'] == $resetPassword['confirmNewPassword']) {
			$this->errors[] = 'Veuillez bien confirmer le même mot de passe';
		}

		return count($this->errors) === 0;
	}
}

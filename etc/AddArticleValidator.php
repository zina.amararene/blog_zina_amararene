<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;
use Framework\Request;

class AddArticleValidator
{
	const KO = 1024;
	const MO = 1048576;

	private $errors = [];
	private $validator;
	private $token;
	private $typePictures = ['image/jpg', 'image/jpeg', 'image/png'];
	private $request;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
		$this->request = new Request();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validatePicture()
	{
		$files = $this->request->getFiles();
		if (!is_uploaded_file($files['imgMaxSize']['tmp_name']) || !is_uploaded_file($files['imgMiniSize']['tmp_name'])) {
			$this->errors[] = 'Problème de transfert d\'images';
		}

		if ($files['imgMaxSize']['size']  > 1*self::MO) {
			$this->errors[] = 'L\'image de taille 900 * 400 px doit être inférieure 1 Mo';
		}

		if ($files['imgMiniSize']['size'] > 200*self::KO) {
			$this->errors[] = 'L\'image de taille 700 * 400 px doit être inférieure 200 Ko';
		}

		if (!in_array($files['imgMaxSize']['type'], $this->typePictures)) {
			$this->errors[] = 'L\'image doit être du type jpg, jpeg ou png';
		}

		if (!in_array($files['imgMiniSize']['type'], $this->typePictures)) {
			$this->errors[] = 'L\'image doit être du type jpg, jpeg ou png';
		}
		return $this->errors;
	}

	public function validate($article)
	{
		if (!$this->token->verifyToken('tokenAddArticle', $article['tokenAddArticle'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isValid($article['title'], 10, 100)) {
            $this->errors[] = 'Le titre doit contenir entre 10 et 100 caractères';
		}

		if (!$this->validator->isValid($article['abstract'], 10, 800)) {
            $this->errors[] = 'Le résumé de l\'article doit contenir entre 10 et 800 caractères';
		}

		if (!$this->validator->isValid($article['content'], 10, 2500)) {
            $this->errors[] = 'Le contenu doit contenir entre 10 et 2500 caractères';
		}

		$this->validatePicture();

		return count($this->errors) === 0;
	}
}

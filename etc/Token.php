<?php

namespace Framework;

use Framework\Session;

class Token
{
	private $session;
	public function __construct()
	{
		$this->session = new Session();
	}
	public function generateToken($tokenName)
	{
		$token = microtime().rand(0,99);
		$token = hash('sha512', $token);
		if ($tokenName == 'tokenCookie') {
		    return $token ;
		}
		$this->session->setSession($tokenName, $token);
		return $token;	
	}

    // Vérifier token du formulaire au token de la session
	public function verifyToken($tokenName, $tokenForm)
	{
		$session = $this->session->getSession();
		if ($session[$tokenName] && isset($tokenForm)) {
			return $session[$tokenName] == $tokenForm ? true : false;
		}
		return false;
	}
}

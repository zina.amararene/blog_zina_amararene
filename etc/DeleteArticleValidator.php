<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class DeleteArticleValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($article)
	{
		if (!$this->token->verifyToken('tokenDeleteArticle', $article['tokenDeleteArticle'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isNumeric($article['idArticle'])) {
            $this->errors[] = 'L\'identifiant de l\'article doit être un chiffre';
		}

		return count($this->errors) === 0;
	}
}

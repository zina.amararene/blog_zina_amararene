<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class CreateRoleValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

    public function validate($role, $userId)
    {
        if (!$this->token->verifyToken('tokenCreateRole', $role['tokenCreateRole'])) {
        	$this->errors[] = 'Token non valide';
        }

        if (!isset($userId) && !\is_numeric($userId)) {
        	$this->errors[] = 'L\'article est inconnu.';
        }

        return count($this->errors) === 0;
    }
}

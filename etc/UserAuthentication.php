<?php

namespace Framework;

use App\DAO\UserDAO;
use Framework\Session;
use Framework\Cookie;

class UserAuthentication
{
    private $userDAO;
    private $session;
    private $cookie;

    public function __construct()
    {
        $this->userDAO = new UserDAO();
        $this->session = new Session();
        $this->cookie = new cookie();
    }

    public function connectedUser()
    {
        $cookieVal = $this->cookie->getCookie();
        if (!empty($cookieVal['user'])) {
            $cookie = json_decode($cookieVal['user']);
            $cookie = (array) $cookie;
            $user= $this->userDAO->verifyUserId($cookie['id']);
            return $user ? $user : false;
        }
        $this->session->setSession('messageSigIn', 'Temps de connexion expiré, veuillez vous auhtentihier');
        header('Location: ../public/index.php?route=signIn');
    }
}

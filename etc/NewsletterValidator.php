<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class NewsletterValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($user)
	{
		if (!$this->token->verifyToken('tokenNewsletter', $user['tokenNewsletter'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isValidEmail($user['email'], 8,50)) {
			$this->errors[] = 'Veuillez indiquer une adresse email valide';
		}

        return count($this->errors) === 0;
	}
}
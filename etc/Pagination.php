<?php

namespace Framework;

class Pagination
{
	//nombre de posts par page
	const ARTICLES_PAGE = 3;

	private $nbPages;

	public function numberOfPages($totalArticles)
	{
		$totalArticles = (int) $totalArticles;
		if ($totalArticles >= 1) {
		    //nombre des pages
		    $pages = $totalArticles / self::ARTICLES_PAGE;
		    $this->nbPages= ceil($pages);
        }
        return $this->nbPages;
	}

	public function firstArticleDisplay($page)
	{
		$page = (int) $page;
		// on test les cas où l'utilisateur peut changer $_GET['page'] et le remplacer par n'importe quoi
		if ($page == 0 || $page > $this->nbPages) {
			$page = 1;
		}
		//On calcul le numéro du premier article a afficher
		$firstArticleDisplay= ($page -1) * self::ARTICLES_PAGE;
		return $firstArticleDisplay;
	}
}

<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class SignInValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrorsSignIn()
	{
		return $this->errors;
	}

    public function validate($identifiers)
    {
    	if (!$this->token->verifyToken('tokenSignIn', $identifiers['tokenSignIn'])) {
    		$this->errors[] = 'Token non valide';
    	}

    	if (preg_match('#@#', $identifiers['userName'])) {
    		if (!$this->validator->isValidEmail($identifiers['userName'], 8, 50)) {
    			$this->errors[] = 'L\'email doit être valide';
    		}
    	}

    	if (!preg_match('#@#', $identifiers['userName'])) {
    		if (!$this->validator->isValid($identifiers['userName'], 3, 50)) {
    			$this->errors[] = 'Le pseudo doit contenir entre 3 et 50 caractères';
    		}
    	}

    	if (!$this->validator->isValidPassword($identifiers['password'], 8, 70)) {
    		$this->errors[] = 'Le mot de passe doit contenir entre 8 et 70 caractères, être composé de minuscules, majuscules et de chiffres';
	    }

	    return count($this->errors) === 0;
    }
}

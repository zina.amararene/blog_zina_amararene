<?php

namespace Framework;

require_once '../vendor/autoload.php';

class Email
{
    // $toReceive vaut true quand un email est reçu
    // $toReceive vaut false quan un email est envoyé via le blog a des utilisateurs

	public function receiveSendEmail(array $sender, $subject, $body, $toReceive, $pathImg)
	{
		$config = require_once '../config/dev_swiftmailler.php';

		$toReceive ? $recipient = $config['mailRecipient'] : $recipient = $sender;
		$toReceive ? $sender = $sender : $sender = $config['mailRecipient'];

		// Create the Transport
		$transport = (new \Swift_SmtpTransport($config['smtp'], $config['port']))
		  ->setUsername($config['username'])
		  ->setPassword($config['password'])
		;

		// Create the Mailer using your created Transport
		$mailer = new \Swift_Mailer($transport);

		// Create a message
		$message = (new \Swift_Message())
		  ->setFrom($sender)
		  ->setTo($recipient)
		  ->setSubject($subject)
		  ->setBody($body, 'text/html')
		  ;
        // if ($pathImg !== null) {
        //     $message->embed(\Swift_Image::fromPath($pathImg));
        // }
		// Send the message
		$result = $mailer->send($message);
		return $result;
	}
}

<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class PostCommentValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($comment)
	{
		if (!$this->token->verifyToken('tokenPostComment', $comment['tokenPostComment'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isNumeric($comment['idComment'])) {
            $this->errors[] = 'L\'identifiant du commentaire doit être un chiffre';
		}

		return count($this->errors) === 0;
	}
}

<?php

namespace Framework;

class ReCaptcha
{

	public function useReCaptcha($response, $remoteip)
	{
		require_once '../config/dev_recaptcha.php';

		$api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
		    . $secret
		    . "&response=" . $response
		    . "&remoteip=" . $remoteip ;

		$decode = json_decode(file_get_contents($api_url), true);
		return $decode['success'] == true ? true : false;
	}
}

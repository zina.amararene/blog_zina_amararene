<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;
use Framework\ReCaptcha;

class RegisterValidator
{
	private $reCaptcha;
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
		$this->reCaptcha = new ReCaptcha();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validateEmail($infosUser)
	{
		if (!$this->validator->isValidEmail($infosUser['email'], 8, 50) || !$this->validator->isValidEmail($infosUser['emailConfirmation'], 8, 50)) {
            $this->errors[] = 'L\'email doit être valide';
	    }

	    if ($infosUser['email'] !== $infosUser['emailConfirmation']) {
	    	$this->errors[] = 'Veuillez confirmer le même email';
	    }

	    if (!$this->validator->isValidPassword($infosUser['password'], 8, 70)) {
	    	$this->errors[] = 'Le mot de passe doit contenir entre 8 et 70 caractères, être composé de minuscules, majuscules et de chiffres';
		}
		return $this->errors;
	}

	public function verifyReCaptcha()
	{
		// Vérification par reCaptcha
	    // if (!$this->reCaptcha->useReCaptcha($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])) {
	    // 	$this->errors[] = 'Le code de vérification est incorrecte';
		// }
		// return $this->errors;
	}

    public function validate($infosUser)
    {
		if (!$this->token->verifyToken('tokenRegister', $infosUser['tokenRegister'])) {
			$this->errors[] = 'Token non valide';
		}

	    if (!$this->validator->isValid($infosUser['firstName'], 3, 100)) {
            $this->errors[] = 'Le prénom doit contenir entre 3 et 100 caractères';
	    }

	    if (!$this->validator->isValid($infosUser['lastName'], 3, 100)) {
            $this->errors[] = 'Le nom doit contenir entre 3 et 100 caractères';
	    }

	    if (!$this->validator->isValid($infosUser['pseudo'], 3, 50) || $this->validator->isNumeric($infosUser['pseudo'])) {
            $this->errors[] = 'Le pseudo doit contenir entre 3 et 50 caractères et ne doit pas contenir que des chiffres';
		}

		$this->validateEmail($infosUser);
		$this->verifyReCaptcha();

	    return count($this->errors) === 0;
    }
}

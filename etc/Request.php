<?php

namespace Framework;

class Request
{
    private $post;
    private $get;
    private $files;
    private $request;
    private $server;

    public function __construct()
    {
        $this->setPost();
        $this->setGet();
        $this->files = $_FILES;
        $this->request = $_REQUEST;
        $this->server = $_SERVER;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function setPost()
    {
        $this->post = filter_input_array(INPUT_POST, $_POST, FILTER_DEFAULT);
    }
 
    public function getGet()
    {
        return $this->get;
    }

    public function setGet()
    {
        $this->get = filter_input_array(INPUT_GET, $_GET, FILTER_DEFAULT);

    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function getEnv($key)
    {
        return getenv($key);
    }

    public function getUri()
    {
        return $this->server['REQUEST_URI'];
    }
}

<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class CommentValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validateArticleDetails($idArticle)
	{
		return (!empty($idArticle) && \is_numeric($idArticle)) ? true : false;
	}

    public function validate($comment, $idArticle)
    {
        if (!$this->token->verifyToken('tokenComment', $comment['tokenComment'])) {
        	$this->errors[] = 'Token non valide';
        }

        if (!isset($idArticle) && !\is_numeric($idArticle)) {
        	$this->errors[] = 'L\'article est inconnu.';
        }

        if (!$this->validator->isValid($comment['userName'], 3, 50)) {
            $this->errors[] = 'Le pseudo doit contenir entre 3 et 50 caractères';
        }

        if (!$this->validator->isValid($comment['contentComment'], 10, 2500)) {
            $this->errors[] = 'Le pseudo doit contenir entre 10 et 2500 caractères';
        }

        return count($this->errors) === 0;
    }
}

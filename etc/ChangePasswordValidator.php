<?php

namespace Framework;

use Framework\Validator;
use Framework\Token;

class ChangePasswordValidator
{
	private $errors = [];
	private $validator;
	private $token;

	public function __construct()
	{
		$this->validator = new Validator();
		$this->token = new Token();
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function validate($password)
	{
		if (!$this->token->verifyToken('tokenChangePassword', $password['tokenChangePassword'])) {
			$this->errors[] = 'Token non valide';
		}

		if (!$this->validator->isValidPassword($password['oldPassword'], 8, 70)) {
            $this->errors[] = 'L\'ancien mot de passe doit contenir entre 8 et 70 caractères, être composé de minuscules, majuscules et de chiffres';
        }
        
        if (!$this->validator->isValidPassword($password['newPassword'], 8, 70)) {
            $this->errors[] = 'Le nouveau mot de passe doit contenir entre 8 et 70 caractères, être composé de minuscules, majuscules et de chiffres';
		}

		if (!$this->validator->isValidPassword($password['confirmNewPassword'], 8, 70)) {
            $this->errors[] = 'Le mot de passe confirmé doit contenir entre 8 et 70 caractères, être composé de minuscules, majuscules et de chiffres';
		}

		if (!$password['newPassword'] == $password['confirmNewPassword']) {
			$this->errors[] = 'Veuillez bien confirmer le même mot de passe';
		}

		return count($this->errors) === 0;
	}
}

<?php

$this->title = 'Blog post | Zina Amararene';
?>

<body style="margin:0; padding:0; background-color:#F2F2F2;">
  <center>
    <table width="100%" border="0" cellpadding="20" cellspacing="0" bgcolor="#F2F2F2">
        <tr>
            <td align="left" valign="top">
              <h4>Madame / Monsieur</h4>
              <p>
                Par la présente, nous tenons à vous notifier notre insatisfaction concernant le contenu inapproprié du
                commentaire que vous avez laisser sur le blog de Zina Amararene, par conséquent il a été supprimé et ne
                sera pas publié.
              </p>
              <p>Nous vous rappelons que tous les commentaires sont soumis à approbation avant leur publlication.</p>
              <p>
                Nous vous adressons un avertissement afin que ces faits ne se reproduisent plus. En cas de nouvel incident,   nous serions dans l'obligation de prendre des sanctions plus sévères à votre encontre.
              </p>
              <p>J’espère que cette démarche engendrera des changements dans votre comportement</p>
              <p>
                Nous vous prions d'agréer, Madame / Monsieur, nos salutations respectueuses.
              </p>
              <strong>Adminstrateur du blog</strong>
            </td>
        </tr>
    </table>
  </center>
</body>
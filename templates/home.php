<?php
$this->title = "Blog | Zina Amararene";

use Framework\Token;
use Framework\Session;

$session = new Session();
$token = new Token();

$message = $session->getSession();
// Générer un token et le stocker en session
$tokenContactMe = $token->generateToken('tokenContactMe');
?>

 <!-- Page Header -->
 <header class="masthead" style="background-image: url('../public/img/plume.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>Mon Blog</h1>
                    <span class="subheading">Developpeuse d'application PHP/Symfony</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 mx-auto">
        <h3>Mon profil en quelques mots</h3>
            <p class="my-profile text-justify">
                Travail avec courage et persévérance, car la ténacité permet d'atteindre l'excellence !
                Il n'y a pas de magie à accomplir, les opportunités ne sont pas offertes, elles doivent être arrachées
                et cela demande de la confiance en soi et du courage. Il faut croire que l'on est doué pour quelque chose et
                il faut l'atteindre coûte que coûte.
            </p>
        </div>
        <div class="col-lg-3 col-md-3 mx-auto">
            <p><img src="../public/img/zina.jpg" alt="Photo de Zina Amararene" class="img-fluid"></p>
        </div>
        <div class="col-lg-10 col-md-10 mx-auto">
            <p class="text-justify">
                Développeuse web curieuse, autonome, rigoureuse, j'aime les applications simples et efficaces.
                Motivée par le besoin de faire toujours mieux et appuyée par des bases acquises lors de mon parcours, je prends plaisir à relever de nouveaux challenges.
            </p>
        </div> 
    </div>  

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto" id="contactMe">
        <h4>Contactez moi</h4>

        <?php if (!empty($message['messageContact'])) : ?>
            <div class="session alert alert-secondary w-75" role="alert">
                <?= htmlspecialchars($message['messageContact']); ?>
            </div>
            <?php $session->unsetSession('messageContact'); ?>
        <?php endif; ?>

        <?php if (isset($errors)): ?>
            <?php foreach($errors as $error): ?>
            <div class="errors alert alert-secondary w-75">
                <?= htmlspecialchars($error); ?>
            </div>
            <?php endforeach; ?>
        <?php endif; ?>

            <form method="post" action="../public/index.php?route=home" name="sentMessage" id="contactForm" novalidate>
                <input type="hidden" name="tokenContactMe" value="<?= htmlspecialchars($tokenContactMe); ?>">
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label for="name">Nom : </label>
                        <input type="text" id="name" name="name" placeholder="Nom *" value="<?= !empty($infosContact) ? htmlspecialchars($infosContact['name']) : ''; ?>" />
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label for="firstName">Prénom : </label>
                        <input type="text" id="firstName" name="firstName" placeholder="Prénom *" value="<?= !empty($infosContact) ? htmlspecialchars($infosContact['firstName']) : ''; ?>"/>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label for="email">Email : </label>
                        <input type="email" id="email" name="email" placeholder="Email *" value="<?= !empty($infosContact) ? htmlspecialchars($infosContact['email']) : ''; ?>"/>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label for="subject">Objet : </label>
                        <input type="text" id="subject" name="subject" placeholder="Objet *" value="<?= !empty($infosContact) ? htmlspecialchars($infosContact['subject']) : ''; ?>" />
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label for="message">Message : </label>
                        <textarea rows="6"  name="message" id="message" class="form-control" placeholder="Message *"><?= !empty($infosContact) ? htmlspecialchars($infosContact['message']) : ''; ?></textarea>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="form-group">
                    <input type="submit" value="Envoyer" name="submit" class="btn btn-primary btn-sm" id="sendMessageButton">
                </div>
            </form>
        </div>
    </div>
</div>

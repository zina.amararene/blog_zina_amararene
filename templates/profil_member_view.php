<?php

$this->title = "Profile utilisateur | Blog Zina Amararene";

use Framework\Cookie;

$cookie = new Cookie();
$cookieUser = $cookie->getCookie();
$cooki = json_decode($cookieUser ['user']);
$cooki = (array) $cooki;
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-bleues.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Mon compte</h1>
                    <span class="subheading">Espace membre</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=myAccount&action=register">Mon compte</a></li>
			<li class="breadcrumb-item active" aria-current="page">Espace membre</li>
		</ol>
	</nav>
	<div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2>Accès membre</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?> </div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
					<a href="../public/index.php?route=myAccount&action=logOut">
					<span style="font-size: 1.2rem;">
						<i class="fas fa-sign-out-alt"></i>
					</span>
					</a>
					</button>
			</div>
		</div>
    </div>
	<div class="row"> 
	    <div class="col-lg-6 col-md-6 offset-md-1 mx-auto">
		    <div class="card">
			    <h4 class="text-center border-bottom w-100 p-3">Votre profile</h4>
				<p class="px-4">
				<i class="far fa-user px-2"></i>
					<a href="../public/index.php?route=userProfile" class="text-decoration-none">Profile</a>
				</p>
				<p class="px-4">
					<i class="far fa-address-card px-2"></i>
					<a href="../public/index.php?route=userAccount&action=changePassword" class="text-decoration-none">Compte</a>
				</p>
				<p class="px-4">
				    <i class="far fa-newspaper px-2"></i>
					<a href="../public/index.php?route=updateArticle&action=showArticlesToUpdate" class="text-decoration-none">Modifier un article</a><br />
					<small class="text-muted">
					    Vous devez disposer des droits requis "Auteur" pour modifier un article
                	</small>
				</p>
		    </div>
		</div>
	</div>
</div>

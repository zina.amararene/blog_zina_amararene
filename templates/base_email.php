<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1; maximum-scale=1.0;">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	    <title><?= $title; ?></title>
	    <style type="text/css">
	    </style>
	</head>
	<?= $content; ?>
</html>
<footer>

</footer>
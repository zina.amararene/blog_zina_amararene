<?php

$this->title = 'Blog post | Zina Amararene';

use Framework\Token;
use Framework\Session;
use Framework\Cookie;

$session = new Session();
$token = new Token();
$cookie = new Cookie();
$cookieUser = $cookie->getCookie();

$message = $session->getSession();
// Générer un token et le stocker en session
$tokenDeleteArticle = $token->generateToken('tokenDeleteArticle');
$cooki = json_decode($cookieUser ['user']);
$cooki = (array) $cooki;
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-noires.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Administartion</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=administration">Espace utilisateur</a></li>
			<li class="breadcrumb-item active" aria-current="page">Modifier ou supprimer un article</li>
		</ol>
    </nav>

	<div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2>Accès administrateur</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?> </div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
					<a href="../public/index.php?route=myAccount&action=logOut">
					<span style="font-size: 1.2rem;">
						<i class="fas fa-sign-out-alt"></i>
					</span>
					</a>
					</button>
			</div>
		</div>
    </div>
    
    <?php if (isset($errors)): ?>
        <?php foreach($errors as $error): ?>
            <p><?=htmlspecialchars($error); ?></p>
        <?php endforeach; ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-12 col-md-12">
		    <div class="col-lg-12 col-md-12">
                <h3>Liste des articles</h3>
            </div>
    
			<?php if (!empty($message['messageArticle'])): ?>
				<?= $message['messageArticle']; ?>
				<?php $session->unsetSession('messageArticle'); ?>
			<?php endif; ?>
           <div class="table-responsive-sm">
				<table class="table table-hover">
					<thead>
						<tr class="text-center">
						<th scope="col">#</th>
						<th scope="col">Auteur</th>
						<th scope="col">Titre</th>
						<th scope="col">Résumé</th>
						<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>

						<?php if (!empty($articles)): ?>
							<?php foreach ($articles as $article): ?>
							<tr>
								<?php $idArticle = $article->getId(); ?>		
									<td class="font-weight-bold"><?= htmlspecialchars($article->getId()); ?></td>
									<td class="text-capitalize"><?= $article->getAuthor(); ?></td>
									<td class="text-center font-weight-bolder"><?= $article->getTitle(); ?></td>
									<td class="text-justify w-50"><?= $article->getAbstract(); ?></td>
								<td class="text-center">
									<a href="../public/index.php?route=updateArticle&action=editArticle&idArticle=<?= htmlspecialchars($idArticle); ?>" class="btn update-article">Modifier</a>
									<a href="../public/index.php?route=updateArticle&action=deleteArticle&idArticle=<?= htmlspecialchars($idArticle); ?>&tokenDeleteArticle=<?= htmlspecialchars($tokenDeleteArticle); ?>" class="btn delete-article">Supprimer</a>
								</td>
							</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
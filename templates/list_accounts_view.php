<?php 

$this->title = "Profile utilisateur | Blog Zina Amararene";

use Framework\Token;
use Framework\Cookie;

$token = new Token();
$cookie = new Cookie();
$cookieUser = $cookie->getCookie();
// Générer un token et le stocker en session
$tokenCreateRole = $token->generateToken('tokenCreateRole');
$cooki = json_decode($cookieUser ['user']);
$cooki = (array) $cooki;
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-bleues.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Administartion</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=administration">Espace utilisateur</a></li>
			<li class="breadcrumb-item active" aria-current="page">Liste des comptes</li>
		</ol>
    </nav>

    <div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2>Accès administrateur</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?> </div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
					<a href="../public/index.php?route=myAccount&action=logOut">
					<span style="font-size: 1.2rem;">
						<i class="fas fa-sign-out-alt"></i>
					</span>
					</a>
					</button>
			</div>
		</div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12">
		    <div class="col-lg-12 col-md-12">
                <h3>Liste des comptes et création des rôles</h3>
            </div>          
            <p><i class="fas fa-check"></i> L'utilisateur est banni</p>
            <p><i class="fas fa-times"></i> L'utilisateur n'est pas dans la liste noire </p>

            <?php if (isset($errors)): ?>
                <?php foreach($errors as $error): ?>
                <div class="errors alert alert-secondary w-75">
                    <?=htmlspecialchars($error); ?>
                </DIV>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="table-responsive-md table-responsive-sm">
                <table class="table table-hover table-scrollable">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th> Nom</th>
                        <th>Prénom</th>
                        <th>Pseudo</th>
                        <th>Rôle</th>
                        <th>Signal</th>
                        <th>Liste noire</th>
                        <th>Création rôle</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($accounts as $account) : ?>
                            <tr>
                            <th scope="row"><?=htmlspecialchars($account->getId()); ?></th>
                            <td class="text-uppercase"><?=htmlspecialchars($account->getLastName()); ?></td>
                            <td class="text-capitalize"><?=htmlspecialchars($account->getFirstName()); ?></td>
                            <td class="text-capitalize"><?=htmlspecialchars($account->getPseudo()); ?></td>
                            <td class="text-capitalize"><?=htmlspecialchars($account->getRole()); ?></td>
                            <td><?=htmlspecialchars($account->getReporting()); ?> </td>
                            <td><?=htmlspecialchars($account->getBlackListId()) != null ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>'; ?></td>
                            <form action="../public/index.php?route=listOfAccounts&action=createRole&userId=<?=htmlspecialchars($account->getId()); ?>" method="post">
                                <input type="hidden" name="tokenCreateRole" value="<?=htmlspecialchars($tokenCreateRole); ?>">
                                <td><select name="role" id="role">
                                    <option value="member" selected>Membre</option>
                                    <option value="author">Auteur</option>
                                    <option value="administrator">Administrateur</option>
                                </select></td>
                                <td><input type="submit" value="Créer" class="btn-input" /></td>
                            </form>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->title = 'Articles | Zina Amararene'; ?>
<?php $articles = $articlesPictures[0]; ?>
<?php $pictures = $articlesPictures[1]; ?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-blanches.jpg')">
    <div class="overlay"></div>
       <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
                <h1>Les articles</h1>
                <span class="subheading">Mon blog</span>
            </div>
        </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
            <li class="breadcrumb-item active" aria-current="page">Blogs posts</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-lg-12 col-md-12 clearfix member">
            <h5>
                <a href="../public/index.php?route=myAccount&action=register" class="float-right font-weight-bold p-2 bd-highlight">Devenir membre</a>
            </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 mx-auto">          
            <?php foreach ($articles as $article) : ?>
                <?php $id = $article->getId(); ?>
                <?php $page ="../public/index.php?route=articleDetails&idArticle=<?= $id; ?>"; ?>
                <?php foreach ($pictures as $picture) : ?>
                    <?php if ($id == $picture->getArticleId()) : ?>
                        <?php $targetPicturePath = $targetDir. '/' . basename($picture->getName()); ?>
                        <div class="post-preview">
                            <img src="<?=$targetPicturePath; ?>" class="card-img-top img-fluid img-article" />
                            <a href="../public/index.php?route=articleDetails&idArticle=<?= $id; ?>">
                                <h2 class="post-title text-center">
                                    <?= $article->getTitle(); ?>
                                </h2>
                                <p class="post-subtitle text-justify">
                                    <?= $article->getAbstract(); ?>
                                </p>
                            </a>
                            <p class="post-meta">Publié par : <?= $article->getAuthor(); ?>
                            <a href="#">Blog Zina Amararene</a>, le : 
                            <?= $article->getDateAdded()->format('d-m-Y'); ?>
                            </p>
                            <div class="clearfix">
                                <a href="../public/index.php?route=articleDetails&idArticle=<?= htmlspecialchars($id); ?>" class="btn btn-primary float-right">Lire la suite</a>
                            </div>             
                        </div>
                        <hr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
            <nav aria-label="...">
                <ul class="pagination pagination-md justify-content-center">
                    <?php for ($i = 1; $i <= $nbPages; $i++) : ?>                           
                        <li class="page-item active" aria-current="page">
                            <span class="page-link">
                                <a href="../public/index.php?route=articles&action=posts&numberPage=<?= $i; ?>">
                                    <?= htmlspecialchars($i); ?>
                                </a>    
                            </span>
                        </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>


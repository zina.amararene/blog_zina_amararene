<?php

$this->title = 'Blog post | Zina Amararene';
?>

<body style="margin:0; padding:0; background-color:#F2F2F2;">
  <center>
    <table width="100%" border="0" cellpadding="20" cellspacing="0" bgcolor="#F2F2F2">
        <tr>
            <td align="left" valign="top">
              <h4>Madame / Monsieur</h4>
              <p>
                Par la présente, nous tenons à vous notifier notre insatisfaction concernant le contenu inapproprié du commentaire que vous continuez a laisser sur le blog de Zina Amararene malgré nos avertissements, par conséquent vous êtes banni du blog.
              </p>
              <p>
                Nous vous prions d'agréer, Madame / Monsieur, nos salutations respectueuses.
              </p>
              <strong>Adminstrateur du blog</strong>
            </td>
        </tr>
    </table>
  </center>
</body>
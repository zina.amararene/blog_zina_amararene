<?php

$this->title = 'Blog post | Zina Amararene';
?>

<body>
  <table width="100%" border="0" cellpadding="20" cellspacing="0" style="border:#000000 solid 1px" bgcolor="#F2F2F2">
    <tr>
      <h1 style="color:#2e93b1; text-align:center">BIENVENUE</h1>
    </tr>
    <tr>
      <div>
        <h2>Bienvenue sur mon blog professionnel, </h2>
        <p style="text-align:justify">
          Je souhaite prendre quelques instants pour vous accueillir en tant que nouveau souscripteur et vous remercier d’être ici !
          Au nom de tous les membres, je vous souhaite la bienvenue et je tiens à ce que vous sachiez que nous sommes
          vraiment ravis que vous ayez décidé de nous rejoindre !
        </p>
      </div>
    </tr>
    <tr>
      <td>
        <div>
            Retrouvons nous sur le
            <strong>
              <a href="http://localhost:8888/openclassrooms/projet5/zina_blog_project/public/index.php" target="_blank" style="color:#2e93b1; text-decoration:none">blog</a>
            </strong>
          </div>
      </td>
    </tr>
  </table>
</body>

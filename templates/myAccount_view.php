<?php

$this->title = 'Mon compte | Blog Zina Amararene';

use Framework\Token;
use Framework\Session;

$session = new Session();
$token = new Token();

$message = $session->getSession();
// Générer un token et le stocker en session
$tokenRegister = $token->generateToken('tokenRegister');
$tokenSignIn = $token->generateToken('tokenSignIn');
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-blanches-dorees.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Mon compte</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item active" aria-current="page">Mon compte</li>
		</ol>
	</nav>
    <div class="row">
        <div class="col-lg-5 col-md-5 offset-md-1 mx-auto">
			<h3>CONNEXION</h3>

			<?php if (!empty($message['messageSigIn'])): ?>
			    <div class="session alert alert-secondary w-75" role="alert">
				    <?= htmlspecialchars($message['messageSigIn']); ?>
				</div>
				<?php   $session->unsetSession('messageSigIn'); ?>
			<?php endif; ?>

			<?php if (isset($errorsSignIn)): ?>
				<?php foreach($errorsSignIn as $error): ?>
				<div class="errors alert alert-secondary w-75">
				    <?= htmlspecialchars($error); ?>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>

			<form action="../public/index.php?route=signIn" method="post">
				<input type="hidden" name="tokenSignIn" value="<?= htmlspecialchars($tokenSignIn); ?>">
				<div class="form-group floating-label-form-group">
					<label for="userName">Utilisateur ou email </label>
					<input type="text" name="userName" id="userName" required placeholder="Pseudo ou email *" value="<?= !empty($identifiers) ? htmlspecialchars($identifiers['userName']) : ''; ?>" />
				</div>
				<div class="form-group floating-label-form-group">
					<label for="password">Mot de passe</label>
					<input type="password" name="password" id ="password" pattern=".{8,}" required title="8 caractère minimum" placeholder="Mot de passe *" value="<?= !empty($identifiers) ? htmlspecialchars($identifiers['password']) : ''; ?>" />
				</div>
				<div class="form-group">
				<p>
				    <input type="checkbox" name="rememberMe" id="rememberMe">
					<label for="rememberMe">Se souvenir de moi</label>
				</p>
				</div>
				<a href="../public/index.php?route=forgotPassword&action=resetPassword">Mot de passe oublié</a>
				<div class="form-group clearfix">
					<input type="submit" class="btn btn-primary btn-sm float-right" value="se connecter" />
				</div>
			</form>
        </div>
    
        <div class="col-lg-6 col-md-6 mx-auto">
			<h3>INSCRIPTION</h3>

			<?php if (!empty($message['messageRegister'])): ?>
			    <div class="session alert alert-secondary w-75" role="alert">
				    <?= htmlspecialchars($message['messageRegister']); ?>
				</div>
				<?php $session->unsetSession('messageRegister'); ?>
			<?php endif; ?>

			<?php if (isset($errors)): ?>
				<?php foreach($errors as $error): ?>
				<div class="errors alert alert-secondary w-75">
				    <?= htmlspecialchars($error); ?>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>

			<form action="../public/index.php?route=myAccount&action=register" method="post">
				<input type="hidden" name="tokenRegister" value="<?= htmlspecialchars($tokenRegister); ?>">
				<div class="form-group floating-label-form-group">
					<label for="firstName">Prénom</label>
					<input type="text" name="firstName" id="firstName" required placeholder="Prénom *" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['firstName']) : ''; ?>" />
				</div>
				<div class="form-group floating-label-form-group">
					<label for="lastName">Nom</label>
					<input type="text" name="lastName" id="lastName" required placeholder="Nom *" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['lastName']) : ''; ?>" />
				</div>
				<div class="form-group floating-label-form-group">
					<label for="pseudo">Pseudonym</label>
					<input type="text" name="pseudo" id="pseudo" required placeholder="Pseudonym *" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['pseudo']) : ''; ?>" />
				</div>

				<div class="form-group floating-label-form-group">
					<label for="email">Adresse email</label>
					<input type="text" name="email" id ="email" required placeholder="Email *" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['email']) : ''; ?>" />
				</div>
				<div class="form-group floating-label-form-group">
					<label for="emailConfirmation">Confirmer adresse email*</label>
					<input type="text" name="emailConfirmation" id ="emailConfirmation" required placeholder="Confirmez email *" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['emailConfirmation']) : ''; ?>" />
				</div>
				<div class="form-group floating-label-form-group">
					<label for="password">Mot de passe</label>
					<input type="password" name="password" id ="password" pattern=".{8,}" required placeholder="Mot de passe *" title="8 caractère minimum" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['password']) : ''; ?>" />
					<!-- <small id="emailHelp" class="form-text text-muted">Minimum 8 caractères</small> -->
					
				</div>
				<p>Minimum 8 caractères</p>
				<div class="g-recaptcha" data-sitekey="6LfI4nMUAAAAAFDEPHF8eUBb3pmu5RiRJ2ds3Qwt"></div>

				<div class="form-group clearfix">
					<input type="submit" class="btn btn-primary btn-sm btn-xs float-right" value="s'inscrire" />
				</div>
			</form>
        </div>
	</div>
</div>


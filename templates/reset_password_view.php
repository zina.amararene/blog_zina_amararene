<?php

use Framework\Token;
use Framework\Session;

$this->title = "Mon compte | Blog Zina Amararene";
$session = new Session();
$token = new Token();

$message = $session->getSession();
// Générer un token et le stocker en session
$tokenUpdatePassword = $token->generateToken('tokenUpdatePassword');
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-dorees.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Mon compte</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=myAccount&action=register">Mon compte</a></li>
			<li class="breadcrumb-item active" aria-current="page">Modification du mot de passe</li>
		</ol>
	</nav>
    <div class="row">
        <div class="col-lg-8 col-md-8 offset-md-1 mx-auto">
		    <div class="col-lg-12 col-md-12">
				<h3>Réinitialisation du mot de passe</h3>
			</div>

			<?php if (!empty($message['messageResetPassword'])): ?>
				<?= htmlspecialchars($message['messageResetPassword']); ?>
				<?php $session->unsetSession('messageResetPassword'); ?>
			<?php endif; ?>

			<?php if (isset($errorsEditPassword)): ?>
				<?php foreach($errorsEditPassword as $error): ?>
					<p><?= htmlspecialchars($error); ?></p>
				<?php endforeach; ?>
			<?php endif; ?>
			<form action="../public/index.php?route=forgotPassword&action=editPassword" method="post" class="col-lg-10 col-md-10">
				<input type="hidden" name ="tokenUpdatePassword" value="<?= htmlspecialchars($tokenUpdatePassword); ?>" />
				<input type="hidden" name ="email" value="<?= htmlspecialchars($email); ?>" />
				<div class="form-group row">
					<label for="newPassword" class="col-sm-6 col-form-label">Nouveau mot de passe *</label>
					<div class="col-sm-6">
						<input type="password" name ="newPassword" id="newPassword" class="px-3 w-100" />
                    </div>
				</div>

				<div class="form-group row">
					<label for="confirmNewPassword" class="col-sm-6 col-form-label">Retapez le mot de passe *</label>
					<div class="col-sm-6">
					    <input type="password" name ="confirmNewPassword" id="confirmNewPassword" class="px-3 w-100" />
                    </div>
				</p>
				<div class="form-group offset-md-9">
					<input type="submit" value ="Modifier" class="btn btn-primary" />
				</div>
			</form>
		</div>
	</div>
</div>

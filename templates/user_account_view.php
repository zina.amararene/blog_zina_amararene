<?php
$this->title = "Administration | Blog Zina Amararene";

use Framework\Token;
use Framework\Session;
use Framework\Cookie;

$session = new Session();
$token = new Token();
$cookie = new Cookie();
$cookieUser = $cookie->getCookie();

$message = $session->getSession();
// Générer un token et le stocker en session
$tokenChangePassword = $token->generateToken('tokenChangePassword');
$tokenChangePseudo = $token->generateToken('tokenChangePseudo');
$tokenDeleteAccount = $token->generateToken('tokenDeleteAccount');
?>
<?php $cooki = json_decode($cookieUser ['user']); ?>
<?php $cooki = (array) $cooki; ?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-noires-dorees.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Administration</h1>
                    <span class="subheading">Mon compte</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=administration">Espace utilisateur</a></li>
			<li class="breadcrumb-item active" aria-current="page">Mon compte</li>
		</ol>
	</nav>
	<div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2>Votre compte</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?> </div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
					<a href="../public/index.php?route=myAccount&action=logOut">
					<span style="font-size: 1.2rem;">
						<i class="fas fa-sign-out-alt"></i>
					</span>
					</a>
					</button>
			</div>
		</div>
    </div>	

    
    <div class="row"> 
        <div class="col-md-12 col-sm-12 offset-md-1">
            <?php if (!empty($message['messageChangePseudo'])): ?>
                <div class="session alert alert-secondary w-75" role="alert">
                    <?= htmlspecialchars($message['messageChangePseudo']); ?>
                </div>
                <?php $session->unsetSession('messageChangePseudo'); ?>
            <?php endif; ?>

            <?php if (isset($errors)): ?>
                <?php foreach($errors as $error): ?>
                <div class="errors alert alert-secondary w-75">
                    <?= htmlspecialchars($error); ?>
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="col-lg-7 col-md-7">
            <div class="col-lg-12 col-md-12">
                <h4>Changer votre mot de passe</h4>
            </div>
            <form action="../public/index.php?route=userAccount&action=changePassword" method="post">
                <input type="hidden" name="tokenChangePassword" value="<?= htmlspecialchars($tokenChangePassword); ?>">
                <div class="form-group">
                    <label for="oldPassword" class="col-sm-5 col-form-label">Ancien mot de passe</label>
                    <input type="password" name="oldPassword" id="oldPassword" class="px-3 w-75" />
                </div>
                <div class="form-group">
                    <label for="newPassword"  class="col-sm-5 col-form-label">Nouveau mot de passe</label>
                    <input type="password" name="newPassword" id="newPassword" class="px-3 w-75" />
                </div>
                <div class="form-group">
                    <label for="confirmNewPassword"  class="col-sm-5 col-form-label">Confirmer mot de passe</label>
                    <input type="password" name="confirmNewPassword" id="confirmNewPassword"  class="px-3 w-75"/>
                </div>
                <input type="submit" value="Valider" class="btn btn-primary"/>
            </form>
        </div>

        <?php if (isset($errorsPseudo)): ?>
            <?php foreach($errorsPseudo as $error): ?>
                <p><?= htmlspecialchars($error); ?></p>
            <?php endforeach; ?>
        <?php endif; ?>
        
        <div class="col-lg-5 col-md-5">
            <div class="col-lg-12 col-md-12">
                <h4>Changer le pseudo</h4>
            </div>
            <form action="../public/index.php?route=userAccount&action=changePseudo" method="post">
            <input type="hidden" name="tokenChangePseudo" value="<?= htmlspecialchars($tokenChangePseudo); ?>">
                <div class="form-group">
                    <label for="newPseudo" class="col-sm-5 col-form-label">Nouveau Pseudo</label>
                    <input type="text" name="newPseudo" id="newPseudo" class="px-3 w-75" />
                </div>
                <div class="form-group offset-md-5"> 
                    <input type="submit" value="Valider" class="btn btn-primary" />
                </div>
                
            </form>
        </div>
    </div>
    
    <div class="row justify-content-md-center"> 
        <div class="col-md-6">
            <div class="col-lg-12 col-md-12"> 
            <hr>  
                <h4 class="text-center">Supprimer mon compte</h4>
            </div>

            <?php if (isset($errorsDeleteAccount)): ?>
                <?php foreach($errorsDeleteAccount as $error): ?>
                    <p><?= htmlspecialchars($error); ?></p>
                <?php endforeach; ?>
            <?php endif; ?>

            <form action="../public/index.php?route=userAccount&action=deleteAccount" method="post">
                <input type="hidden" name="tokenDeleteAccount" value="<?= htmlspecialchars($tokenDeleteAccount); ?>">
                <div class="form-group">
                    <label for="newPseudo" class="col-sm-2 col-form-label">Email</label>
                    <input type="text" name="email" id="email" class="px-3 w-75" /> 
                    <div class="form-group offset-md-8">    
                        <input type="submit" value="Valider" class="btn btn-primary"/>  
                    </div>    
                </div>
            </form>
        </div>
    </div>
</div>
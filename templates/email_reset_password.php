<?php

$this->title = 'Blog post | Zina Amararene';
?>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <table bgcolor="#fff0e3" width="100%" border="0" cellpadding="20" cellspacing="0">
        <tbody>
        <tr>
        <td>
            <h2>Réinitialisation de votre mot de passe sur le blog de Zina Amararene</h2>
            <h4> Bonjour,</h4>
            <p>Vous avez demandé la réinitialisation d’un mot de passe. </p>
            <p>Pour procéder à cette réinitialisation, veuillez cliquer sur le lien ci-dessous. </p>

    	    <p>
                <a href="http://localhost:8888/openclassrooms/projet5/zina_blog_project/public/index.php?route=forgotPassword&action=passwordRecovery&token=<?= htmlspecialchars($tokenResetPassword); ?>">
                <strong>Réinitialiser</strong>
                </a>
    	    </p>
            <p>Si vous n’êtes pas à l’origine de cette demande, vous pouvez ignorer cet email.</p>
            <strong>Cordialement</strong>
        </td>
        </tr>
        </tbody>
    </table>
</body>

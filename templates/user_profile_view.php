<?php
$this->title = "Profile utilisateur | Blog Zina Amararene";

use Framework\Token;
use Framework\Session;
use Framework\Cookie;

$session = new Session();
$token = new Token();
$cookie = new Cookie();
$cookieUser = $cookie->getCookie();
$message = $session->getSession();
// Générer un token et le stocker en session
$tokenUserProfile = $token->generateToken('tokenUserProfile');
?>
<?php $cooki = json_decode($cookieUser ['user']); ?>
<?php $cooki = (array) $cooki; ?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-noires-dorees.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Administration</h1>
                    <span class="subheading">Mon profile</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=administration">Espace utilisateur</a></li>
			<li class="breadcrumb-item active" aria-current="page">Mon profile</li>
		</ol>
	</nav>
	<div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2>Votre Profile</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?></div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
				    <a href="../public/index.php?route=myAccount&action=logOut">
						<span style="font-size: 1.2rem;">
							<i class="fas fa-sign-out-alt"></i>
						</span>
					</a>
				</button>
			</div>
		</div>
    </div>	

   <div class="row">
		<div class="col-lg-12 col-md-12">
			<h4>Modification de vos informations personnelles</h4>
		</div>

		<?php if (!empty($message['messageUserProfile'])): ?>
		    <div class="session alert alert-secondary w-75" role="alert">
			    <?= htmlspecialchars($message['messageUserProfile']); ?>
			</div>
			<?php $session->unsetSession('messageUserProfile'); ?>
	    <?php endif; ?>

		<?php if (isset($errors)): ?>
			<?php foreach($errors as $error): ?>
				<div class="errors alert alert-secondary w-75">
					<?= htmlspecialchars($error); ?>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
		
		<form action="../public/index.php?route=userProfile" method="post" class="col-lg-10 col-md-10">
			<input type="hidden" name="tokenUserProfile" value="<?= htmlspecialchars($tokenUserProfile); ?>" />
			<div class="form-group row">
				<label for="firstName" class="col-sm-3 col-form-label">Prénom *</label>
				<div class="col-sm-8">
					<input type="text" name="firstName" id="firstName" palceholder="Prénom" class="px-3 w-75" required value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['first_name']) : ''; ?>" />
				</div>
			</div>
			<div class="form-group row">
				<label for="lastName" class="col-sm-3 col-form-label">Nom *</label>
				<div class="col-sm-8">
					<input type="text" name="lastName" id="lastName" required  class="px-3 w-75" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['last_name']) : ''; ?>" />
				</div>
			</div>	
			<div class="form-group row">
				<label for="email" class="col-sm-3 col-form-label">Adresse email *</label>
				<div class="col-sm-8">
					<input type="text" name="email" id ="email" required class="px-3 w-75" value="<?= !empty($infosUser) ? htmlspecialchars($infosUser['email']) : ''; ?>" />
				</div>
			</div>
			<div class="form-group row">
				<label for="emailConfirmation" class="col-sm-3 col-form-label">Confirmer email *</label>
				<div class="col-sm-8">
					<input type="text" name="emailConfirmation" id ="emailConfirmation" class="px-3 w-75" required />
				</div>
			</div>
			<div class="form-group offset-md-7">
				<input type="submit" value="Modifier" class="btn btn-primary"/>
			</div>  
		</form>
		</div>
    </div>
</div>
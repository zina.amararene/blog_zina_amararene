<?php

$this->title = 'Blog post | Zina Amararene';

use Framework\Token;
use Framework\Session;

$token = new Token();
$session = new Session();

$message = $session->getSession();
// Générer un token et le stocker en session
$tokenComment = $token->generateToken('tokenComment');
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-dorees.jpg')">
    <div class="overlay"></div>
       <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1 class="text-center">Détail article </h1>
                    <h2 class="subheading text-center">Explorer - Connaitre</h2>
                </div>
           </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
	
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=articles&action=posts">Blogs posts</a></li>
			<li class="breadcrumb-item active" aria-current="page">Détail post</li>
		</ol>
	</nav>

	<div class="row">
        <div class="col-lg-12 col-md-12 clearfix member">
            <h5>
                <a href="../public/index.php?route=myAccount&action=register" class="float-right font-weight-bold p-2 bd-highlight">Devenir membre</a>
            </h5>
        </div>
	</div>
	
	<div class="row">	
		<div class="col-lg-10 col-md-10 mx-auto">
			<?php $article = $articleDetails[0]; ?>
			<?php $picture = $articleDetails[1]; ?>

			<?php $targetPicturePath = $targetDir. '/' . basename($picture->getName()); ?>
			<p class="w-100 text-center"><img class="img-fluid" src="<?= $targetPicturePath; ?>"/></p>
			<?php $id = $article->getId(); ?>
			<h1 class="text-center"> <?= $article->getTitle(); ?> </h1>
			<div class="text-justify font-weight-bolder font-italic font-weight-bold m-b-md">
				<?= $article->getAbstract(); ?>
			</div>
			<div class="text-justify space-20">
				<?= nl2br($article->getContent()); ?>
			</div>
			<p class="font-weight-bold text-right"> Écrit par : 
				<span class="text-capitalize"><?= $article->getAuthor(); ?></span>
			</p>
			<div class="font-weight-bold text-right">
				<?= !empty($article->getDateModification()) ? sprintf('Modifié le : %s', $article->getDateModification()->format('d-m-Y')) : ''; ?>
            </div>

			<?php if (!empty($comments)) :?>
				<h3>Vos commentaires</h3>
				<?php foreach ($comments as $comment): ?>
					<p class="px-4">
						<span class="text-capitalize font-weight-bold pseudo-comment"><?= $comment->getPseudo(); ?></span>
					    <?= $comment->getDateAdded()->format('d-m-Y'); ?>
				    </p>
					<p> <?= $comment->getContent(); ?> </p>
				<?php endforeach; ?>
			<?php endif ?>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-10 col-md-10 mx-auto" id="comment">	
			
			<h3>Laisser un commentaire</h3>

			<?php if (!empty($message['messageComment'])): ?>
				<div class="session alert alert-secondary w-75" role="alert">
					<?= $message['messageComment']; ?>
				</div>
				<?php $session->unsetSession('messageComment'); ?>
			<?php endif; ?>

			<?php if (isset($errors)): ?>
				<?php foreach($errors as $error): ?>
					<div class="errors alert alert-secondary w-75">
						<?= $error; ?>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>

			<div class="col-lg-10 col-md-10 mx-auto">	
				<form action="../public/index.php?route=articleDetails&idArticle=<?= $id ?>" method="post">				    
						<input type="hidden" name="tokenComment" value="<?= $tokenComment; ?>" />
						<div class="form-group">
							<p><label for="userName">Votre pseudo *</label></p>
							<input type="text" name="userName" id="userName" class="w-50" />
						</div>
						<div class="form-group">
							<p><label for="contentComment">Votre commentaire *</label></p>
							<textarea name="contentComment" id="contentComment" class="form-control" rows="10"></textarea>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Publier" />
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


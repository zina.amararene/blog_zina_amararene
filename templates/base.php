<?php

use Framework\Token;
use Framework\Session;

$session = new Session();
$token = new Token();

$message = $session->getSession();
// Générer un token et le stocker en session
$tokenNewsletter = $token->generateToken('tokenNewsletter');
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= htmlspecialchars($title); ?></title>
    <!-- Bootstrap core CSS -->
    <link href="../public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="../public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
    <link href="../public/css/clean-blog.min.css" rel="stylesheet">
    <link href="../public/css/style.css" rel="stylesheet">
    <!-- trumbowyg -->
    <link rel="stylesheet" href="../public/trumbowyg/dist/ui/trumbowyg.min.css">
  </head>

	<body>
    <header>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <!-- <a class="navbar-brand" href="../public/index.php">Zina Amararene</a> -->
          <a href="../public/index.php" class="navbar-brand">
            <img src="../public/img/logo/logo-zina.png" class="d-inline-block align-top logo" width="30" height="30" alt="Logo Amararene">
            Zina Amararene
          </a>
          <!-- <a class="navbar-brand" href="../public/index.php">Zina Amararene</a> -->
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="../public/index.php?route=home">Accueil</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../public/index.php?route=articles&action=posts">Blogs post</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../public/index.php?route=myAccount&action=register">Mon compte</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

		<section><?= $content; ?></section>

		<hr>

		 <!-- Footer -->
		<footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 mx-auto">
            <h5>S'inscrire à la newsletter</h5>
            <?php if (!empty($message['messageNewsletter'])): ?>
              <?= htmlspecialchars($message['messageNewsletter']); ?>
              <?php $session->unsetSession('messageNewsletter'); ?>
            <?php endif; ?>
            
            <form action="../public/index.php?route=newsletter" method="post">
              <div class="control-group">
                <div class="form-group input-group-prepend">
                  <input type="hidden" name="tokenNewsletter" value="<?= htmlspecialchars($tokenNewsletter); ?>">
                  <div class="input-group-text rounded-0" id="btnGroupAddon">@</div>
                  <input type="email" name="email" placeholder="Email">
                  <input type="submit" class="btn-primary" value="OK" />
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-6 col-md-6 mx-auto">
            <h5 class="text-center">Suivez-moi, partout !</h5>
            <ul class="list-inline text-center">
              <li class="list-inline-item">
                <a href="https://twitter.com/rene_zina">
                  <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://fr.linkedin.com/in/zina-amararene-291b2858">
                  <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-linkedin fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://gitlab.com/zina.amararene">
                  <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-gitlab fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://github.com/ZAmararene">
                  <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 mx-auto">
            <p class="copyright text-muted">2018/2019 - Zina Amararene - Tous droits réservés - Mentions légales</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../public/vendor/jquery/jquery.min.js"></script>
    <script src="../public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ReCaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- Custom scripts for this template -->
    <script src="../public/js/clean-blog.min.js"></script>
    <!-- trumbowyg -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
    <script src="../public/trumbowyg/dist/trumbowyg.min.js"></script>
    <script src="../public/trumbowyg/dist/plugins/upload/trumbowyg.cleanpaste.min.js"></script>
    <script src="../public/trumbowyg/dist/plugins/upload/trumbowyg.pasteimage.min.js"></script>
    <script src="../public/trumbowyg/dist/langs/fr.min.js"></script>  
    <script src="../public/js/main.js"></script>
	</body>
</html>
<?php

$this->title = "Mon compte | Blog Zina Amararene";

use Framework\Token;
use Framework\Session;

$session = new Session();
$message = $session->getSession();
$token = new Token();
// Générer un token et le stocker en session
$tokenForgotPassword = $token->generateToken('tokenForgotPassword');

?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-dorees.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Mon compte</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=myAccount&action=register">Mon compte</a></li>
			<li class="breadcrumb-item active" aria-current="page">Modification du mot de passe</li>
		</ol>
	</nav>
    <div class="row">
        <div class="col-lg-8 col-md-8 offset-md-1 mx-auto">
		    <div class="col-lg-12 col-md-12">
			    <h2>Réinitialiser votre mot de passe </h2>
			</div>
			<p>Envoyez votre adresse e-mail et nous vous renverrons un lien pour réinitialiser votre mot de passe</p>

			<?php if (!empty($message['messageForgotPassword'])): ?>
				<?= htmlspecialchars($message['messageForgotPassword']); ?>
				<?php $session->unsetSession('messageForgotPassword'); ?>
			<?php endif; ?>

			<?php if (isset($errorsResetPassword)): ?>
				<?php foreach($errorsResetPassword as $error): ?>
					<p><?= htmlspecialchars($error); ?></p>
				<?php endforeach; ?>
			<?php endif; ?>
            <div class="col-lg-10 col-md-10">
				<form action="../public/index.php?route=forgotPassword&action=resetPassword" method="post">
					<input type="hidden" name="tokenForgotPassword" value="<?= htmlspecialchars($tokenForgotPassword); ?>">
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="text" name="email" placholder="Email">
						<input type="submit" value="Soumettre" class="btn btn-primary btn-sm" />
					</div>
				</form>
            </div>
			<div>
				<p>J'ai déjà un identifiant et un mot de passe : <a href="../public/index.php?route=signIn">Se connecter</a></p>
			</div>
		</div>
	</div>
</div>
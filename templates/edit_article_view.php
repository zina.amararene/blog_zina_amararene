<?php

$this->title = 'Blog post | Zina Amararene';

use Framework\Token;
use Framework\Session;

$token = new Token();
$session = new Session();
$message = $session->getSession();
// Générer un token et le stocker en session
$tokenEditArticle = $token->generateToken('tokenEditArticle');

?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-bleues.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Administartion</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=administration">Espace utilisateur</a></li>
            <li class="breadcrumb-item"><a href="../public/index.php?route=administration#update">Mettre à jour les articles</a></li>
            <li class="breadcrumb-item"><a href="../public/index.php?route=updateArticle&action=showArticlesToUpdate">Modifier ou supprimer un article</a></li>
			<li class="breadcrumb-item active" aria-current="page">Modifier un article</li>
		</ol>
    </nav>

    <div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2>Accès administrateur</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?> </div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
					<a href="../public/index.php?route=myAccount&action=logOut">
					<span style="font-size: 1.2rem;">
						<i class="fas fa-sign-out-alt"></i>
					</span>
					</a>
					</button>
			</div>
		</div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
		    <div class="col-lg-12 col-md-12">
                <h3>Modifier un article</h3>
            </div>

            <?php if (!empty($message['messageArticle'])): ?>
                <div class="session alert alert-secondary w-75" role="alert">
                    <?= htmlspecialchars($message['messageArticle']); ?>
                </div>
                <?php $session->unsetSession('messageArticle'); ?>
            <?php endif; ?>

            <?php if (isset($errors)): ?>
                <?php foreach($errors as $error): ?>
                    <div class="errors alert alert-secondary w-75">
                        <?= htmlspecialchars($error); ?>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        <div class="row">
            <div class="col-lg-10 col-md-10">
                <form action="../public/index.php?route=updateArticle&action=editArticle&idArticle=<?= $article->getId() ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="tokenEditArticle" value="<?= htmlspecialchars($tokenEditArticle); ?>" />
                    <input type="hidden" name="oldTitle" value="<?= htmlspecialchars($article->getTitle()); ?>" />
                    <div class="form-group">
                        <p class="font-weight-bold"><label for="title">Titre *</label></p>
                        <input type="text" name="title" id="title" value="<?= htmlspecialchars($article->getTitle()); ?>" class="w-100" />
                    </div>
                    <div class="form-group">
                        <p class="font-weight-bold"><label for="abstract">Résumé *</label></p>
                        <textarea name="abstract" id="abstract" class="form-control" rows="7">
                            <?= htmlspecialchars($article->getAbstract()); ?>
                        </textarea>
                    </div>
                    <div class="form-group">
                        <p class="font-weight-bold"><label for="content">Contenu *</label></p>
                        <textarea name="content" id="content" class="form-control" rows="12">
                            <?= htmlspecialchars($article->getContent()); ?>
                        </textarea>
                    </div>
                    <p class="col-lg-12 col-md-12 col-sm-12 font-weight-bold">Images à uploader format (jpeg, jpg ou png)</p>
                    <div class="form-group">
                        <input type="file" name="imgMaxSize" id="imgMaxSize" accept=".jpg, .jpeg, .png" />
                        <small class="text-muted">
                            Image de 900 * 400 px. Taille max : 1 Mo
                        </small>
                    </div>
                    <div class="form-group">
                        <input type="file" name="imgMiniSize" id="imgMiniSize" accept=".jpg, .jpeg, .png" />
                        <small class="text-muted">
                            Image de 700 * 400 px. Taille max : 200 Ko
                        </small>
                    </div>
                    <div class="clearfix">
                        <input type="submit" value="Modifier l'article" class="btn btn-primary float-right" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
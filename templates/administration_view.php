<?php

$this->title = "Profile utilisateur | Blog Zina Amararene";

use Framework\Session;
use Framework\Cookie;

$session = new Session();
$cookie = new Cookie();
$cookieUser = $cookie->getCookie();
$message = $session->getSession();
?>

<?php $cooki = json_decode($cookieUser ['user']); ?>
<?php $cooki = (array) $cooki; ?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-noires-dorees.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Administration</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=myAccount&action=register">Mon compte</a></li>
			<li class="breadcrumb-item active" aria-current="page">Espace Administrateur</li>
		</ol>
	</nav>

    <div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2 class="text-center">Accès administrateur</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?> </div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
					<a href="../public/index.php?route=myAccount&action=logOut">
						<span style="font-size: 1.2rem;">
							<i class="fas fa-sign-out-alt"></i>
						</span>
					</a>
				</button>
			</div>
		</div>
    </div>

	<?php if (!empty($message['messageAdministration'])): ?>
		<?= htmlspecialchars($message['messageAdministration']); ?>
		<?php $session->unsetSession('messageAdministration'); ?>
	<?php endif; ?>
     
	<div class="row"> 
	    <div class="col-lg-6 col-md-6  m-s-ms">
			<div class="card">
				<h4 class="text-center border-bottom w-100 p-3">Votre profile</h4>
				<p class="px-4">
					<i class="fas fa-user px-2"></i>
					<a href="../public/index.php?route=userProfile" class="text-decoration-none">Profile</a>
				</p>
				<p class="px-4">
					<i class="far fa-address-card px-2"></i>
					<a href="../public/index.php?route=userAccount&action=changePassword" class="text-decoration-none">Compte</a>
				</p>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 m-s-ms">
			<div class="card">
				<h4 class="text-center border-bottom w-100 p-3">Gérer les comptes</h4>
				<p class="px-4">
					<i class="fas fa-users px-2"></i>
					<a href="../public/index.php?route=listOfAccounts&action=listOfAccounts" class="text-decoration-none">Liste des comptes | création des rôles</a>
				</p>	
				<p class="px-4">
					<i class="fab fa-creative-commons-by px-2"></i>
					<a href="#" class="text-decoration-none">Attribuer des droits d'accès</a>
				</p>
		    </div>
		</div>
	</div>
	<div class="row"> 
	    <div class="col-lg-6 col-md-6 m-s-ms">
		    <div class="card">
				<h4 class="text-center border-bottom w-100 p-3">Gérer les contenus</h4>
				<p class="px-4">
					<i class="fas fa-comments px-2"></i>
					<a href="../public/index.php?route=validateComments&action=postComment" class="text-decoration-none">Voir et valider des commentaires</a>
				</p>
				<p class="px-4">
					<i class="far fa-envelope-open px-2"></i>
					<a href="#" class="text-decoration-none">Créer l'email de la newsletter</a>
				</p>
		    </div>
		</div>
        <div class="col-lg-6 col-md-6 m-s-ms">
		    <div class="card">
				<h4 class="text-center border-bottom w-100 p-3">Mettre à jour les articles</h4>
				<p class="px-4">
					<i class="fas fa-newspaper px-2"></i>
					<a href="../public/index.php?route=addArticle" class="text-decoration-none">Ajouter un article</a>
				</p>
				<p class="px-4">
					<i class="far fa-newspaper px-2"></i>
					<a href="../public/index.php?route=updateArticle&action=showArticlesToUpdate" class="text-decoration-none">Modifier ou supprimer un article</a>
				</p>
			</div>
		</div>
	</div>
</div>
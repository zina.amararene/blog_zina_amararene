<?php

$this->title = 'Administration | Blog Zina Amararene';

use Framework\Token;
use Framework\Session;
use Framework\Cookie;

$token = new Token();
$session = new Session();
$cookie = new Cookie();
$cookieUser = $cookie->getCookie();
$message = $session->getSession();

// Générer un token et le stocker en session
$tokenPostComment = $token->generateToken('tokenPostComment');
$tokenRemoveComment = $token->generateToken('tokenRemoveComment');
$cooki = json_decode($cookieUser ['user']);
$cooki = (array) $cooki;
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/plumes-vertes.jpg')">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Administartion</h1>
                    <span class="subheading">Ma contribution au blog</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../public/index.php?route=home">Accueil</a></li>
			<li class="breadcrumb-item"><a href="../public/index.php?route=administration">Espace utilisateur</a></li>
			<li class="breadcrumb-item active" aria-current="page">Validation des commentaires</li>
		</ol>
    </nav>

	<div class="row"> 
	    <div class="col-lg-6 col-md-6 col-sm-6 col-8 offset-md-2 p-3"> 
			<h2>Accès administrateur</h2>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-4">
		    <div class="clearfix">
				<?php if (!empty($cooki)) : ?>
					<div class="float-right text-capitalize font-weight-bold p-3"><?= htmlspecialchars($cooki['firstName']); ?> </div>
				<?php endif; ?>
			</div>
			<div class="clearfix">
				<button type="button" class="btn btn-sm shadow p-3 mb-5 bg-white float-right" data-toggle="tooltip" data-placement="left" title="Se déconnecter">
					<a href="../public/index.php?route=myAccount&action=logOut">
					<span style="font-size: 1.2rem;">
						<i class="fas fa-sign-out-alt"></i>
					</span>
					</a>
				</button>
			</div>
		</div>
    </div>
     
	<div class="row">
	    <div class="col-md-12 col-sm-12 offset-md-1">
			<?php if (!empty($message['messageCommentValidate'])): ?>
				<div class="session alert alert-secondary w-75" role="alert">
					<?= htmlspecialchars($message['messageCommentValidate']); ?>
				</div>
				<?php $session->unsetSession('messageCommentValidate'); ?>
			<?php endif; ?>

			<?php if (isset($errors)): ?>
				<?php foreach($errors as $error): ?>
				<div class="errors alert alert-secondary w-75">
				    <?= htmlspecialchars($error); ?>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
        <div class="col-lg-12 col-md-12">
		    <div class="col-lg-12 col-md-12">
                <h3>Validation des commentaires</h3>
            </div>

            <div class="table-responsive-md table-responsive-sm">
                <table class="table table-hover table-scrollable">
                    <thead>
                        <tr class="text-center">
                        <th>#</th>
                        <th>Pseudo</th>
                        <th>Contenu</th>
						<th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php if (!empty($commentsToValidate)): ?>
							<?php foreach ($commentsToValidate as $comment): ?>
								<tr>
									<td class="font-weight-bold"><?= htmlspecialchars($comment->getId()); ?></td>
									<td class="text-center text-capitalize"><?= htmlspecialchars($comment->getPseudo()); ?></td>
									<td class="text-justify"><?= htmlspecialchars($comment->getContent()); ?></td>
									<td class="text-center">
										<div>
											<a href="../public/index.php?route=validateComments&action=postComment&idComment=<?= htmlspecialchars($comment->getId()); ?>&tokenPostComment=<?= htmlspecialchars($tokenPostComment); ?>" class="btn edit-comment">Editer</a>
										</div>
										<div>
											<a href="../public/index.php?route=validateComments&action=removeComment&idComment=<?= htmlspecialchars($comment->getId()); ?>&pseudo=<?= htmlspecialchars($comment->getPseudo()); ?>&tokenRemoveComment=<?= htmlspecialchars($tokenRemoveComment); ?>" class="btn delete-comment">Supprimer</a>
										</div>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

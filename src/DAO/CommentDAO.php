<?php

namespace App\DAO;

use Framework\DAO\DAO;
use App\Models\Comment;

class CommentDAO extends DAO
{
	private $dbb;

	public function __construct()
	{
		$this->dbb = $this->connectionDBB();
	}

	public function addComment(Comment $comment, $toPublish)
	{
		$req = $this->dbb->prepare(
			'INSERT INTO comment(pseudo, content, to_publish, undesirable, date_added, article_id)
			VALUES(:pseudo, :content, :toPublish, :undesirable, :dateAdded, :articleId)');
		$req->bindValue(':pseudo', $comment->getPseudo(), \PDO::PARAM_STR);
		$req->bindValue(':content', $comment->getContent(), \PDO::PARAM_STR);
		$req->bindValue(':toPublish', $toPublish, \PDO::PARAM_BOOL);
		$req->bindValue(':undesirable', $comment->getUndesirable(), \PDO::PARAM_BOOL);
		$req->bindValue(':dateAdded', $comment->getDateAdded()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
		$req->bindValue(':articleId', $comment->getArticleId(), \PDO::PARAM_INT);
		$req->execute();
	}

	public function getComments($idArticle)
	{
		$req = $this->dbb->prepare(
			'SELECT id, pseudo, content, to_publish, undesirable, date_added, article_id
			FROM comment
			WHERE article_id= :idArticle AND to_publish= :toPublish ORDER BY id DESC');
		$req->bindValue(':idArticle', $idArticle, \PDO::PARAM_INT);
		$req->bindValue(':toPublish', 1, \PDO::PARAM_BOOL);
		$req->setFetchMode(\PDO::FETCH_ASSOC);
		$req->execute();
		$data = $req->fetchAll();
		$comments = [];
		foreach($data as $value) {
		    $comment = new Comment();
		    $comment->hydrate($value);
		    $comments[] = $comment;
		}
		return $comments;
	}

	public function getCommentsToValidate()
	{
		$req = $this->dbb->query(
			'SELECT id, pseudo, content, to_publish, undesirable, date_added, article_id
			FROM comment
			WHERE to_publish = false');
		$req->setFetchMode(\PDO::FETCH_CLASS, Comment::class);
		$req->execute();
		return $req->fetchAll();
	}

	public function validateComment($idComment)
	{
		$req = $this->dbb->prepare('UPDATE comment SET to_publish= :toPublish WHERE id= :idComment');
		$req->bindValue(':toPublish', 1, \PDO::PARAM_BOOL);
		$req->bindValue(':idComment', $idComment, \PDO::PARAM_INT);
		$req->execute();
	}

	public function removeComment($idComment)
	{
		$req = $this->dbb->prepare('DELETE FROM comment WHERE id= :idComment');
		$req->bindValue(':idComment', $idComment, \PDO::PARAM_INT);
		$req->execute();
	}
}

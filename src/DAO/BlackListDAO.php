<?php

namespace App\DAO;

use Framework\DAO\DAO;
use App\Models\BlackList;

class BlackListDAO extends DAO
{
	private $dbb;

	public function __construct()
	{
		$this->dbb = $this->connectionDBB();
	}

	public function addBlackList(BlackList $blackList)
	{
		$req = $this->dbb->prepare('INSERT INTO blackList(email_user, number_warning) VALUES(:email, :reporting)');
		$req->bindValue(':email', $blackList->getEmailUser(), \PDO::PARAM_STR);
		$req->bindValue(':reporting', $blackList->getNumberWarning(), \PDO::PARAM_INT);
		$req->execute();
		return $this->dbb->lastInsertId();
	}

	public function memberIsBlacklisted($email)
	{
        $req = $this->dbb->prepare('SELECT * FROM blackList WHERE email_user= :email');
        $req->bindValue(':email', $email, \PDO::PARAM_STR);
        $req->execute();
        return $req->fetch();
	}
}

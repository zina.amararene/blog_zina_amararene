<?php

namespace App\DAO;

use src\model\Article;
use Framework\DAO\DAO;
use App\Models\Visitor;

class NewsletterDAO extends DAO
{
	private $dbb;

	public function __construct()
	{
		$this->dbb = $this->connectionDBB();
	}

	public function existsEmail($email)
	{
		$sql = $this->dbb->prepare('SELECT id, email, unsubscribe FROM visitor WHERE email = :email');
		$sql->bindValue(':email', $email, \PDO::PARAM_STR);
		$sql->execute();
		return $sql->fetch();
	}

	public function addVisitor(Visitor $visitor)
	{
		$sql = $this->dbb->prepare('INSERT INTO visitor(email, unsubscribe) VALUES(:email, :unsubscribe)');
		$sql->bindValue(':email', $visitor->getEmail(), \PDO::PARAM_STR);
		$sql->bindValue(':unsubscribe', $visitor->getUnsubscribe(), \PDO::PARAM_BOOL);
		$sql->execute();
	}
}

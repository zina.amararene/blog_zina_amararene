<?php

namespace App\DAO;

use Framework\DAO\DAO;
use App\Models\Article;
use App\Models\Picture;

class ArticleDAO extends DAO
{
	private $dbb;

	public function __construct()
	{
		$this->dbb = $this->connectionDBB();
	}

	public function getArticles($firstPostDisplay, $nbPages)
	{
		$req = $this->dbb->prepare(
			'SELECT article.id, article.title, article.author, article.abstract, article.content, article.date_added,
			article.date_modification, article.user_id, picture.name, picture.image_size, picture.image_type, picture.article_id
			FROM article AS article, picture AS picture
			WHERE article.id = picture.article_id
			AND picture.name LIKE :search
			ORDER BY article.id DESC LIMIT :firstPostDisplay, :nbPages');
		$req->bindValue(':firstPostDisplay', $firstPostDisplay, \PDO::PARAM_INT);
		$req->bindValue(':nbPages', $nbPages, \PDO::PARAM_INT);
		$req->bindValue(':search', '%_mini%', \PDO::PARAM_STR);
		$req->execute();
		$req->setFetchMode(\PDO::FETCH_ASSOC);
		$data = $req->fetchAll();
		$articles = [];
		$pictures = [];
		foreach ($data as $value) {
			$article = new Article();
			$picture = new Picture();
			$article->hydrate($value);
			$picture->hydrate($value);
			$articles[] = $article;
			$pictures[] = $picture;
		}
		return array($articles, $pictures);
	}

	public function getTotalArticles()
	{
		$req = $this->dbb->query('SELECT COUNT(id) AS total FROM article');
        return $req->fetch();
	}

	public function getArticle($idArticle)
	{
		$req = $this->dbb->prepare(
			'SELECT id, title, author, abstract, content, date_added, date_modification, user_id
            FROM article
            WHERE id= :idArt');
		$req->bindValue(':idArt', $idArticle, \PDO::PARAM_INT);
		$req->setFetchMode(\PDO::FETCH_ASSOC);
		$req->execute();
		$data = $req->fetch();
		$article = new Article();
		$article->hydrate($data);
		return $article;
	}

	public function getDetails($idArticle)
	{
		$req = $this->dbb->prepare(
			'SELECT article.id, article.title, article.author, article.abstract, article.content, article.date_added,
			article.date_modification, article.user_id, picture.name, picture.image_size, picture.image_type, picture.article_id
            FROM article AS article, picture AS picture
            WHERE article.id= :idArt
            AND article.id = picture.article_id
            AND picture.name LIKE :search
        ');
		$req->bindValue(':idArt', $idArticle, \PDO::PARAM_INT);
		$req->bindValue(':search', '%_max%', \PDO::PARAM_STR);
		$req->setFetchMode(\PDO::FETCH_ASSOC);
		$req->execute();
		$data = $req->fetch();
		$article = new Article();
		$picture = new Picture();
		$article->hydrate($data);
		$picture->hydrate($data);
		return array($article, $picture);
	}

	public function existTitle($title)
	{
		$req = $this->dbb->prepare('SELECT * FROM article WHERE title= :title');
		$req->bindValue(':title', $title, \PDO::PARAM_STR);
		$req->execute();
		return $req->fetch();
	}

	public function addArticle(Article $article) {
		$req = $this->dbb->prepare(
			'INSERT INTO article(title, author, abstract, content, date_added, date_modification, user_id)
			VALUES(:title, :author, :abstract, :content, :dateAdded, :dateModification, :userId)');
		$req->bindValue(':title', $article->getTitle(), \PDO::PARAM_STR);
		$req->bindValue(':author', $article->getAuthor(), \PDO::PARAM_STR);
		$req->bindValue(':abstract', $article->getAbstract(), \PDO::PARAM_STR);
		$req->bindValue(':content', $article->getContent(), \PDO::PARAM_STR);
		$req->bindValue(':dateAdded', $article->getDateAdded()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
		$req->bindValue(':dateModification', $article->getDateModification(), \PDO::PARAM_STR);
		$req->bindValue(':userId', $article->getUserId(), \PDO::PARAM_INT);
        $req->execute();
	}

	public function getArticlesToUpdate()
	{
		$req = $this->dbb->query(
			'SELECT id, title, author, abstract, content, date_added, date_modification, user_id
			FROM article
			ORDER BY id DESC');
		$req->setFetchMode(\PDO::FETCH_ASSOC);
		$data = $req->fetchAll();
		$articles = [];
		foreach ($data as $value) {
			$article = new Article();
			$article->hydrate($value);
			$articles[] = $article;
		}
		return $articles;
	}

	public function editArticle(Article $article, $articleId)
	{
		$req = $this->dbb->prepare(
			'UPDATE article
			SET title= :title, author= :author, abstract= :abstract, content= :content, date_modification= :dateModification
			WHERE id= :articleId');
		$req->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
		$req->bindValue(':title', $article->getTitle(), \PDO::PARAM_STR);
		$req->bindValue(':author', $article->getAuthor(), \PDO::PARAM_STR);
		$req->bindValue(':abstract', $article->getAbstract(), \PDO::PARAM_STR);
		$req->bindValue(':content', $article->getContent(), \PDO::PARAM_STR);
		$req->bindValue(':dateModification', $article->getDateModification()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
        $req->execute();
	}

	public function deleteArticle($articleId)
	{
		$req = $this->dbb->prepare('DELETE FROM article WHERE id= :id');
		$req->bindValue(':id', $articleId, \PDO::PARAM_INT);
        $req->execute();
	}
}

<?php

namespace App\DAO;

use Framework\DAO\DAO;
use App\Models\Picture;

class PictureDAO extends DAO
{
    private $dbb;

    public function __construct()
    {
        $this->dbb = $this->connectionDBB();
    }

    public function addPicture(Picture $picture, $articleId)
    {
        $req = $this->dbb->prepare(
            'INSERT INTO picture(name, image_size, image_type, article_id)
	    VALUES(:name, :imageSize, :imageType, :articleId)'
        );
        $req->bindValue(':name', $picture->getName(), \PDO::PARAM_STR);
        $req->bindValue(':imageSize', $picture->getImageSize(), \PDO::PARAM_INT);
        $req->bindValue(':imageType', $picture->getImageType(), \PDO::PARAM_STR);
        $req->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
        $req->execute();
    }

    public function editPicture(Picture $picture, $articleId, $search)
    {
        $req = $this->dbb->prepare(
            'UPDATE  picture
            SET name= :name, image_size= :imageSize, image_type= :imageType
            WHERE article_id= :articleId
            AND name LIKE :search
        ');
        $req->bindValue(':name', $picture->getName(), \PDO::PARAM_STR);
        $req->bindValue(':imageSize', $picture->getImageSize(), \PDO::PARAM_INT);
        $req->bindValue(':imageType', $picture->getImageType(), \PDO::PARAM_STR);
        $req->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
        $req->bindValue(':search', '%'.$search.'%', \PDO::PARAM_STR);
        $req->execute();
    }
}

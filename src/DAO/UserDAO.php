<?php

namespace App\DAO;

use Framework\DAO\DAO;
use App\Models\User;

class UserDAO extends DAO
{
	private $dbb;

	public function __construct()
	{
		$this->dbb = $this->connectionDBB();
	}

	public function existUser($pseudo, $email)
	{
		$req = $this->dbb->prepare(
			'SELECT *
			FROM user
			WHERE (pseudo= :pseudo AND email= :email) Or pseudo= :pseudo OR email= :email'
		);
		$req->bindValue(':pseudo', $pseudo, \PDO::PARAM_STR);
		$req->bindValue(':email', $email, \PDO::PARAM_STR);
		$req->execute();
		return $req->fetch();
	}

	public function verifyUserId($userId)
	{
		$req = $this->dbb->prepare('SELECT * FROM user WHERE SHA2(id, 256)= :userId');
		$req->bindValue(':userId', $userId, \PDO::PARAM_INT);
		$req->execute();
		return $req->fetch();
	}

	public function addUser(User $user)
	{
		$req = $this->dbb->prepare(
			'INSERT INTO user(last_name, first_name, pseudo, email, password, reset_password, role, super_admin, reporting, date_connection, blackList_id, newsletter_id)
		    VALUES(:lastName, :firstName, :pseudo, :email, :password, :resetPassword, :role, :superAdmin, :reporting, :dateConnection, :blackListId, :newsletterId)'
		);
		$req->bindValue(':lastName', $user->getLastName(), \PDO::PARAM_STR);
		$req->bindValue(':firstName', $user->getFirstName(), \PDO::PARAM_STR);
		$req->bindValue(':pseudo', $user->getPseudo(), \PDO::PARAM_STR);
		$req->bindValue(':email', $user->getEmail(), \PDO::PARAM_STR);
		$req->bindValue(':password', $user->getPassword(), \PDO::PARAM_STR);
		$req->bindValue(':resetPassword', $user->getResetPassword(), \PDO::PARAM_STR);
		$req->bindValue(':role', $user->getRole(), \PDO::PARAM_STR);
		$req->bindValue(':superAdmin', $user->getSuperAdmin(), \PDO::PARAM_BOOL);
		$req->bindValue(':reporting', $user->getReporting(), \PDO::PARAM_INT);
		$req->bindValue(':dateConnection', $user->getDateConnection()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
		$req->bindValue(':blackListId', $user->getBlackListId(), \PDO::PARAM_INT);
		$req->bindValue(':newsletterId', $user->getNewsletterId(), \PDO::PARAM_INT);
		$req->execute();
	}

	public function getUser($userName)
	{
		$req = $this->dbb->prepare('SELECT * FROM user WHERE pseudo= :userName OR email= :userName');
		$req->bindValue(':userName', $userName, \PDO::PARAM_STR);
		$req->execute();
		return $req->fetch();
	}

	public function addTokenResetPassword($tokenResetPassword, $idUser)
	{
		$req = $this->dbb->prepare('UPDATE user SET reset_password= :tokenPassword WHERE id= :id');
		$req->bindValue(':tokenPassword', $tokenResetPassword, \PDO::PARAM_STR);
		$req->bindValue(':id',$idUser, \PDO::PARAM_INT);
		$req->execute();
	}

	public function verifyTokenRestPassword($token)
	{
		$req = $this->dbb->prepare('SELECT * FROM user WHERE reset_password= :token');
		$req->bindValue(':token', $token, \PDO::PARAM_STR);
		$req->execute();
		return $req->fetch();
	}

	public function removeTokenResetPassword($userId)
	{
		$req = $this->dbb->prepare('UPDATE user SET reset_password= :resetPassword WHERE id= :id');
		$req->bindValue(':id', $userId, \PDO::PARAM_INT);
		$req->bindValue(':resetPassword', NULL, \PDO::PARAM_STR);
		$req->execute();
	}

	public function editPassword($userId, $userEmail, $newPassword)
	{
		$req = $this->dbb->prepare('UPDATE user SET password= :newPassword WHERE id= :userId AND email= :userEmail');
		$req->bindValue(':newPassword', $newPassword, \PDO::PARAM_STR);
		$req->bindValue(':userId', $userId, \PDO::PARAM_INT);
		$req->bindValue(':userEmail', $userEmail, \PDO::PARAM_STR);
		$req->execute();
	}

	public function editReporting($userId, $reporting)
	{
		$req = $this->dbb->prepare('UPDATE user SET reporting= :reporting WHERE id=:userId');
		$req->bindValue(':userId', $userId, \PDO::PARAM_INT);
		$req->bindValue(':reporting', $reporting, \PDO::PARAM_INT);
		$req->execute();
	}

	public function editPseudo($pseudo, $userId)
	{
		$req = $this->dbb->prepare('UPDATE user SET pseudo= :pseudo WHERE id= :userId');
		$req->bindValue(':userId', $userId, \PDO::PARAM_INT);
		$req->bindValue(':pseudo', $pseudo, \PDO::PARAM_STR);
		$req->execute();
	}

	public function updateUserInfo($lastName, $firstName, $email, $userId)
	{
		$req = $this->dbb->prepare(
			'UPDATE user 
			SET last_name= :lastName, first_name= :firstName, email= :email 
			WHERE id= :userId');
		$req->bindValue(':lastName', $lastName, \PDO::PARAM_STR);
		$req->bindValue(':firstName',$firstName, \PDO::PARAM_STR);
		$req->bindValue(':email', $email, \PDO::PARAM_STR);
		$req->bindValue(':userId', $userId, \PDO::PARAM_INT);
		$req->execute();
	}

	public function banUser($idUser, $idBlackList)
	{
		$req = $this->dbb->prepare('UPDATE user SET blackList_id= :blackListId WHERE id= :idUser');
		$req->bindValue(':idUser', $idUser, \PDO::PARAM_INT);
		$req->bindValue(':blackListId', $idBlackList, \PDO::PARAM_INT);
		$req->execute();
	}

	public function deleteAccount($userId)
	{
		$req = $this->dbb->prepare('DELETE FROM user WHERE id= :userID');
		$req->bindValue(':userID', $userId, \PDO::PARAM_INT);
		$req->execute();
	}

	public function getUsers()
	{
		$req = $this->dbb->query('SELECT * FROM user ORDER BY last_name');
		$req->setFetchMode(\PDO::FETCH_ASSOC);
		$data = $req->fetchAll();
		$users = [];
		foreach($data as $value) {
			$user = new User();
			$user->hydrate($value);
			$users[] = $user;
		}
		return $users;
	}

	public function userExists($userId)
	{
		$req = $this->dbb->prepare('SELECT * FROM user WHERE id= :userId');
		$req->bindValue(':userId', $userId, \PDO::PARAM_INT);
		$req->execute();
		return $req->fetch();
	}

	public function createRole($role, $userId)
	{
		$req = $this->dbb->prepare('UPDATE user SET role= :role WHERE id= :userId');
		$req->bindValue(':userId', $userId, \PDO::PARAM_INT);
		$req->bindValue(':role', $role, \PDO::PARAM_STR);
		$req->execute();
	}
}
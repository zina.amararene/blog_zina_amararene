<?php

namespace App\Controllers;

use Framework\View;
use App\DAO\UserDAO;
use Framework\UserProfileValidator;
use\Framework\UserAuthentication;
use Framework\Session;

class UserProfileController
{
	private $view;
	private $userDAO;
    private $userProfiledValidator;
    private $userAuthentication;
    private $session;

	public function __construct()
	{
		$this->view = new View();
		$this->userDAO = new UserDAO();
        $this->userProfileValidator = new UserProfileValidator();
        $this->userAuthentication = new UserAuthentication();
        $this->session = new Session();
    }

    public function editProfile($infoUserToEdit)
    {
        $user = $this->userAuthentication->connectedUser();
        if ($user) {
            if (!empty($infoUserToEdit) && $this->userProfileValidator->validate($infoUserToEdit)) {                   
                $this->userDAO->updateUserInfo($infoUserToEdit['lastName'], $infoUserToEdit['firstName'], $infoUserToEdit['email'], $user['id']);
                $this->session->setSession('messageUserProfile', 'Votre profil à été modifié');
                header('Location: ../public/index.php?route=userProfile'); 
                exit;        
            }
        }
        $this->view->render('user_profile_view', [
            'infosUser' => $user,
            'errors' => $this->userProfileValidator->getErrors()
        ]);
    }
}

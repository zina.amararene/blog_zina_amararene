<?php

namespace App\Controllers;

use App\DAO\ArticleDAO;
use App\DAO\PictureDAO;
use Framework\View;
use App\Models\Article;
use App\DAO\UserDAO;
use App\Models\Picture;
use Framework\AddArticleValidator;
use\Framework\UserAuthentication;
use Framework\Session;
use Framework\Request;

class AddArticleController
{
	private $view;
	private $articleDAO;
	private $pictureDAO;
	private $article;
	private $picture;
	private $userDAO;
	private $addArticleValidator;
	private $userAuthentication;
	private $session;
	private $request;

	public function __construct()
	{
		$this->articleDAO = new ArticleDAO();
		$this->pictureDAO = new PictureDAO();
		$this->view = new View();
		$this->article = new Article();
		$this->picture = new Picture();
		$this->userDAO = new UserDAO();
		$this->addArticleValidator = new AddArticleValidator();
		$this->userAuthentication = new UserAuthentication();
		$this->session = new Session();
		$this->request = new Request();
	}

    // Récuperer les informations des images de l'article et les inserer dans la table Picture
	public function transfertPictures($pictureName, $articleId)
	{
		$files = $this->request->getFiles();
		$picture = [
		    'name' => $files[$pictureName]['name'],
		    'imageSize' => $files[$pictureName]['size'],
		    'imageType' => $files[$pictureName]['type']
		];

        $this->picture->create($picture, $articleId);
        $this->pictureDAO->addPicture($this->picture, $articleId);
	}

	public function addArticle($article)
	{
		$authenticatedUser = $this->userAuthentication->connectedUser();
		if ($authenticatedUser && $authenticatedUser['role'] == 'administrator') {
			if (!empty($article) && $this->addArticleValidator->validate($article)) {
				if (!$this->articleDAO->existTitle($article['title'])) {
					$this->article->create(
						$article['title'],
						$authenticatedUser['pseudo'],
						$article['abstract'],
						$article['content'],
						$authenticatedUser['id']
					);
					$this->articleDAO->addArticle($this->article);
                    $articleAdded = $this->articleDAO->existTitle($article['title']);
					$this->transfertPictures('imgMaxSize', $articleAdded['id']);
					$this->transfertPictures('imgMiniSize', $articleAdded['id']);
                    $this->session->setSession('messageArticle', 'L\'article à bien été ajouté.');
					header('Location: ../public/index.php?route=addArticle');
					exit;
				}
				if ($this->articleDAO->existTitle($article['title'])) {
				    $this->session->setSession('messageArticle', 'Le titre existe déjà, il faut en choisir un autre');
				}
			}
		}
	    $this->view->render('add_article_view', [
	    	'article' => $article,
	    	'errors' => $this->addArticleValidator->getErrors()
	    ]);
	}
}

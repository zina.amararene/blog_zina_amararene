<?php

namespace App\Controllers;

use Framework\View;

class errorController
{
	private $view;

	public function __construct()
	{
		$this->view = new View();
	}

	public function unknown()
	{
		$this->view->render('unknown', []);
	}

	public function error()
	{
		$this->view->render('error', []);
	}
}

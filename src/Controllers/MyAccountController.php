<?php

namespace App\Controllers;

use Framework\View;
use App\DAO\UserDAO;
use App\Models\User;
use Framework\Email;
use App\DAO\BlackListDAO;
use Framework\RegisterValidator;
use Framework\Session;

class MyAccountController
{
	private $view;
	private $userDAO;
	private $user;
	private $email;
	private $blackListDAO;
	private $registerValidator;
	private $session;

	public function __construct()
	{
		$this->view = new View();
		$this->userDAO = new UserDAO();
		$this->email = new Email();
		$this->user = new User();
		$this->blackListDAO = new BlackListDAO();
		$this->registerValidator = new RegisterValidator();
		$this->session = new Session();
	}

	public function logOut()
	{
		$this->session->destroySession();
		header('Location: ../public/index.php?route=home');
	}

	public function register($infosUser)
	{
		if (!empty($infosUser) && $this->registerValidator->validate($infosUser)) {
		    // Vérifier si l'utilisateur figure dans la liste noire
            $memberIsBlacklisted = $this->blackListDAO->memberIsBlacklisted($infosUser['email']);
                // Vérifier si email et pseudo du nouveau utilistaeur existe
			$existUser = $this->userDAO->existUser($infosUser['pseudo'], $infosUser['email']);
			if (!$existUser && !$memberIsBlacklisted) {
				$password = password_hash($infosUser['password'], PASSWORD_DEFAULT);
				$this->user->create(
					$infosUser['lastName'],
					$infosUser['firstName'],
					$infosUser['pseudo'],
					$infosUser['email'],
					$password
				);
				$this->userDAO->addUser($this->user);
				// Envoyer au nouveau memebre un email de bienvenue
				$sender = [$infosUser['email']];
				$subject = 'Votre inscription sur le blog Zina Amararene est validée';
				$cid = 'http://localhost:8888/openclassrooms/projet5/zina_blog_project/public/img/emailing/carte.png';
				$body = $this->view->renderEmail('email_welcome', ['cid' => $cid]);
				$this->email->receiveSendEmail($sender, $subject, $body, false, $cid);
				$this->session->setSession('messageRegister', 'Ravie de vous compter parmi les membres du blog. Veuillez vous authentifier pour accéder à votre compte.');
				header('Location: ../public/index.php?route=signIn');
				exit;
			} elseif ($existUser['pseudo'] == $infosUser['pseudo'] && $existUser['email'] == $infosUser['email']) {
				$this->session->setSession('messageRegister', 'Le pseudo et l\'email existent déjà, veuillez en choisir un d\'autres');
			} elseif ($existUser['pseudo'] == $infosUser['pseudo']) {
				$this->session->setSession('messageRegister', 'Le pseudo existent déjà, veuillez en choisir un autre');
			} elseif ($existUser['email'] == $infosUser['email']) {
				$this->session->setSession('messageRegister', 'L\'email existent déjà, veuillez en choisir un autre');
			}
			
			if ($memberIsBlacklisted) {
			    $this->session->setSession('messageRegister', 'Vous avez été banni, vous ne pouvez pas vous inscrire.');
			}
        }
        $this->view->render('myAccount_view', [
        	'infosUser' => $infosUser,
        	'errors' => $this->registerValidator->getErrors()
        ]);
	}
}

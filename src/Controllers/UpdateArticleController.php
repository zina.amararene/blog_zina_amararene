<?php

namespace App\Controllers;

use App\DAO\ArticleDAO;
use App\DAO\PictureDAO;
use Framework\View;
use App\Models\Article;
use App\DAO\UserDAO;
use App\Models\Picture;
use Framework\EditArticleValidator;
use Framework\DeleteArticleValidator;
use\Framework\UserAuthentication;
use Framework\Session;
use Framework\Request;

class UpdateArticleController
{
	private $view;
	private $articleDAO;
	private $pictureDAO;
	private $article;
	private $picture;
	private $userDAO;
	private $editArticleValidator;
	private $deleteArticleValidator;
	private $userAuthentication;
	private $session;
	private $request;

	public function __construct()
	{
		$this->articleDAO = new ArticleDAO();
		$this->pictureDAO = new PictureDAO();
		$this->view = new View();
		$this->article = new Article();
		$this->picture = new Picture();
		$this->userDAO = new UserDAO();
		$this->editArticleValidator = new EditArticleValidator();
		$this->deleteArticleValidator = new DeleteArticleValidator();
		$this->userAuthentication = new UserAuthentication();
		$this->session = new Session();
		$this->request = new Request();
	}

    // Récuperer les informations des images de l'article et les inserer dans la table Picture
	public function transfertPictures($pictureName, $articleId, $search)
	{
		$files = $this->request->getFiles();
		$picture = [
		    'name' => $files[$pictureName]['name'],
		    'imageSize' => $files[$pictureName]['size'],
		    'imageType' => $files[$pictureName]['type']
		];

		$this->picture->create($picture, $articleId);
        $this->pictureDAO->editPicture($this->picture, $articleId, $search);
	}

	// Afficher tous les articles contenant un lien de modification et suppression pour chaque article,
	// uniquement pour personnes dont habilité à mettre à jour les articles qui sont :
	// Administrateur et hauteur
	public function showArticlesToUpdate()
	{
		$authenticatedUser = $this->userAuthentication->connectedUser();
		if ($authenticatedUser['role'] == 'administrator' || $authenticatedUser['role'] == 'author') {
			$articles = $this->articleDAO->getArticlesToUpdate();
			$this->view->render('list_articles_update_view', ['articles' => $articles]);
		} else {
		    header('Location: ../public/index.php?route=administration');
	    }
	}

    public function editArticle($article, $articleId)
    {
		$files = $this->request->getFiles();
		$authenticatedUser = $this->userAuthentication->connectedUser();
    	if ($authenticatedUser) {
    		$datailArticle = $this->articleDAO->getArticle($articleId);
			if (!empty($article) && $this->editArticleValidator) {
		    	$articleExist = $this->articleDAO->existTitle($article['oldTitle']);
		    	if ($articleExist) {
		    		$this->article->update($article['title'], $authenticatedUser['pseudo'], $article['abstract'], $article['content']);
		    		$this->articleDAO->editArticle($this->article, $articleExist['id']);
		    		// Modifier les images uniquement si téléchargement de celles-ci
		    		if (is_uploaded_file($files['imgMaxSize']['tmp_name'])) {
                        $this->transfertPictures('imgMaxSize',$articleId, '_max');
		    		}
		    		if (is_uploaded_file($files['imgMiniSize']['tmp_name'])) {
		    			$this->transfertPictures('imgMiniSize', $articleId, '_mini');
		    		}
					$this->session->setSession('messageArticle', 'L\'article à bien été modifié');
					header('Location: ../public/index.php?route=updateArticle&action=showArticlesToUpdate');
					exit;
				}
				if (!$articleExist) {
					$this->session->setSession('messageArticle', 'Le titre n\'existe pas pour le modifier, il faut en choisir un autre');
					header('Location: ../public/index.php?route=updateArticle&action=showArticlesToUpdate');
				}
			}
        }
    	$this->view->render('edit_article_view', [
    		'article' => $datailArticle,
    		'errors' => $this->editArticleValidator->getErrors()

    	]);
    }

    public function deleteArticle($article)
    {
		if (!empty($article) && $this->deleteArticleValidator->validate($article)) {
			$authenticatedUser = $this->userAuthentication->connectedUser();
			if ($authenticatedUser && $authenticatedUser['role'] == 'administrator') {			
				if ($this->articleDAO->getArticle($article['idArticle'])) {
					$this->articleDAO->deleteArticle($article['idArticle']);
					$this->session->setSession('messageArticle', 'L\'article à été supprimé');
					header('Location: ../public/index.php?route=articles&action=showArticlesToUpdate');
					exit;
				}
				$this->session->setSession('messageArticle', 'L\'article n\'existe pas, il faut en choisir un autre');
			}
			$this->session->setSession('messageArticle', 'Vous n\'avez pas les droits requis pour suuprimer un article');
			header('Location: ../public/index.php?route=updateArticle&action=showArticlesToUpdate');
		}
		header('Location: ../public/index.php?route=updateArticle&action=showArticlesToUpdate');
    }
}

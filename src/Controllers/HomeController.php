<?php

namespace App\Controllers;

use Framework\View;
use Framework\Email;
use Framework\ContactMeValidator;
use Framework\Session;

class HomeController
{
	private $view;
    private $contactMeValidator;
	private $email;
	private $session;

	public function __construct()
	{
		$this->view = new View();
        $this->contactMeValidator = new ContactMeValidator();
		$this->email = new Email();
        $this->session = new Session();
	}

    // Affichage de la page d'accueil et traitement des emails envoyés par les utilisateurs
	public function home($contact)
	{
		if (!empty($contact) && $this->contactMeValidator->validate($contact)) {
		    $sender = [$contact['email'] => sprintf('%s %s', $contact['name'], $contact['firstName'])];
			$body = $contact['message'];
			$subject = $contact['subject'];
		    $this->email->receiveSendEmail($sender, $subject, $body, true, null);
			$this->session->setSession('messageContact', 'votre message nous est bien parvenu');
			header('Location: ../public/index.php?route=home#contactMe');
			exit;
		}

		$this->view->render('home', [
			'infosContact' => $contact,
			'errors' => $this->contactMeValidator->getErrors()
		]);
	}
}

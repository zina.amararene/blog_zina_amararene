<?php

namespace App\Controllers;

use Framework\View;
use App\DAO\UserDAO;
use Framework\Cookie;
use Framework\SignInValidator;
use\Framework\UserAuthentication;
use Framework\Session;

class SignInController
{
	private $view;
	private $userDAO;
	private $cookie;
	private $signInValidator;
	private $userAuthentication;
	private $session;

	public function __construct()
	{
		$this->view = new View();
		$this->userDAO = new UserDAO();
		$this->cookie = new Cookie();
		$this->signInValidator = new SignInValidator();
		$this->userAuthentication = new UserAuthentication();
		$this->session = new Session();
	}

	public function administration()
	{
		$user = $this->userAuthentication->connectedUser();
		if ($user) {
			if ($user['role'] == 'administrator') {
				$this->view->render('administration_view', []);
			}
			if ($user['role'] != 'administrator') {
			    $this->view->render('profil_member_view', []);
		    }
		} 		
	}

	public function signIn($identifiers)
	{
		if (!empty($identifiers) && $this->signInValidator->validate($identifiers)) {
            $user = $this->userDAO->getUser($identifiers['userName']);
			$is_password_correct = password_verify($identifiers['password'], $user['password']);

			if ($user['pseudo'] && $is_password_correct) {
				// COOKIE
				$cookie = ['id' => hash('sha256', $user['id']),
					'firstName' => $user['first_name']
				];
                // Génération du temps d'expiration  du cookie pour une periode de 3 heures
			    $expirationTime = time()+ 3*3600;
			    // Se souvenir de moi
			    if (isset($identifiers['rememberMe'])) {
					// Génération du temps d'expiration du cookie pour une periode indéterminée
					$expirationTime = null;
			    } 
			    $this->cookie->setCookie($cookie, 'user', $expirationTime);
			    header('Location: ../public/index.php?route=administration');
			}
			$this->session->setSession('messageSigIn', 'Utisateur inconnu ou mot de passe incorrecte');
		}

		$this->view->render('myAccount_view', [
			'identifiers' => $identifiers,
			'errorsSignIn' => $this->signInValidator->getErrorsSignIn()
		]);
	}
}

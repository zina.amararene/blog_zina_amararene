<?php

namespace App\Controllers;

use App\DAO\ArticleDAO;
use Framework\View;
use Framework\Pagination;

class ArticlesController
{
	private $view;
	private $articleDAO;

	public function __construct()
	{
		$this->articleDAO = new ArticleDAO();
		$this->view = new View();
		$this->pagination = new Pagination();
	}

    public function articles($numberPage)
	{
		$totalArticles = $this->articleDAO->getTotalArticles();
		$totalArticles = $totalArticles['total'];
		$nbPages = $this->pagination->numberOfPages($totalArticles);
		// On récupère le numéro de la page indiqué dans l'adresse
		if (isset($numberPage)) {
			// le cas où la vraiable ne contient pas d'entier, on transforme $numberPage en entier qui vaut 0
			$page = (int) $numberPage;
			$firstPostDisplay = $this->pagination->firstArticleDisplay($page);
		}
		if (!isset($numberPage)) {
			//il s'agit ici de la première page
			$page = 1;
			$firstPostDisplay = $this->pagination->firstArticleDisplay($page);
		}
        $articlesPictures = $this->articleDAO->getArticles($firstPostDisplay, Pagination::ARTICLES_PAGE);
        $targetDir = '../public/img/img_article/img_mini';
		$this->view->render(
			'articles_view',
			['articlesPictures' =>  $articlesPictures, 'nbPages' => $nbPages, 'targetDir' => $targetDir]
		);
	}
}

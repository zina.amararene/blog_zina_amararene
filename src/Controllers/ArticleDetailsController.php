<?php

namespace App\Controllers;

use App\DAO\ArticleDAO;
use Framework\View;
use App\DAO\UserDAO;
use App\DAO\CommentDAO;
use App\Models\Article;
use App\Models\Comment;
use Framework\CommentValidator;
use Framework\Session;

class ArticleDetailsController
{
	private $view;
	private $articleDAO;
	private $userDAO;
	private $commentDAO;
	private $comment;
	private $commentValidator;
	private $session;

	public function __construct()
	{
		$this->articleDAO = new ArticleDAO();
		$this->view = new View();
		$this->userDAO = new UserDAO();
		$this->commentDAO = new CommentDAO();
		$this->comment = new Comment();
		$this->commentValidator = new CommentValidator();
		$this->session = new Session();
	}

    // // Affichage détail d'un articles, l'ensemble de ses commentaires et ajouter des commentaires
	public function articleDetails($comment, $idArticle)
	{
	    $articleDetails = $this->articleDAO->getDetails($idArticle);
	    $comments = $this->commentDAO->getComments($idArticle);
        $targetDir = '../public/img/img_article/img_max';
	    if (!empty($comment) && $this->commentValidator->validate($comment, $idArticle)) {
	    	$user = $this->userDAO->getUser($comment['userName']);
	    	if ($articleDetails) {
	    		// Vérifier si le membre n'est pas banni
				if ($user['reporting'] < 3) {
					if ($user && $user['pseudo'] == $comment['userName']) {
						$this->comment->create(
                            	$comment['userName'],
                            	$comment['contentComment'],
                            	$idArticle
                            );
						if ($user['role'] == 'administrator') {
							$toPublish = true;
                            $message = 'Le commentaire a été ajouté';
                        } else {
							$toPublish= false;
                            $message = 'Le commentaire est en attente de validation avant sa publication';
						}
						$this->commentDAO->addComment($this->comment, $toPublish);
						$this->session->setSession('messageComment', $message);
						header('location: ../public/index.php?route=articleDetails&idArticle=' . $idArticle.'#comment');
						exit;
					} else {
						$this->session->setSession('messageComment', 'Vous n\'êtes pas membre, vous pouvez vous inscrire pour pouvoir publier des commentaires');
                    }
                } else {
                    $this->session->setSession('messageComment', 'Vous êtes banni du blog, vous n\'avez plus le droit de publier des commentaires');
                }
			} else {
        	    $this->session->setSession('messageComment', 'L\'article n\'existe pas.');
            }
        }
        // Affichage par défaut de l'article et ces commentaires
        $this->view->render('article_details_view', [
        	'articleDetails' => $articleDetails,
        	'comments' => $comments,
        	'targetDir' => $targetDir,
            'errors' => $this->commentValidator->getErrors()
        ]);
	}
}

<?php

namespace App\Controllers;

use Framework\View;
use App\DAO\UserDAO;
use Framework\ChangePasswordValidator;
use Framework\ChangePseudoValidator;
use Framework\DeleteAccountValidator;
use\Framework\UserAuthentication;
use Framework\Session;

class UserAccountController
{
	private $view;
	private $userDAO;
	private $changePasswordValidator;
	private $changePseudoValidator;
	private $deleteAccountValidator;
	private $userAuthentication;
	private $session;

	public function __construct()
	{
		$this->view = new View();
		$this->userDAO = new UserDAO();
		$this->changePasswordValidator = new ChangePasswordValidator();
		$this->changePseudoValidator = new ChangePseudoValidator();
		$this->deleteAccountValidator = new DeleteAccountValidator();
		$this->userAuthentication = new UserAuthentication();
		$this->session = new Session();
    }
    
    public function changePassword($password)
    {
		if (!empty($password) && $this->changePasswordValidator->validate($password)) {
			$user = $this->userAuthentication->connectedUser();
			if ($user && password_verify($password['oldPassword'], $user['password'])) {
				$newPassword = password_hash($password['newPassword'], PASSWORD_DEFAULT);
				$this->userDAO->editPassword($user['id'], $user['email'], $newPassword);
				$this->session->setSession('messageSigIn', 'Le mot de passe à été échangé, utiliser le pour vous connecter');
				header('Location: ../public/index.php?route=signIn');
				exit;
			} 
		}
        $this->view->render('user_account_view', ['errors' => $this->changePasswordValidator->getErrors()]);
	}
	
	public function changePseudo($userInfo)
	{
		$user = $this->userAuthentication->connectedUser();
		if ($user) {
			if (!empty($userInfo) && $this->changePseudoValidator->validate($userInfo)) {
				if (!$this->userDAO->getUser($userInfo['newPseudo'])) {
					$this->userDAO->editPseudo($userInfo['newPseudo'], $user['id']);
					$this->session->setSession('messageChangePseudo', 'Le nouveau pseudo à bien été pris en compte');
					header('Location: ../public/index.php?route=userAccount&action=changePseudo');
					exit;
				}
				if ($this->userDAO->getUser($userInfo['newPseudo'])) {
					$this->session->setSession('messageChangePseudo', 'Le pseudo existe déjà, veuillez en choisir un autre');
				}
			}
		}
		$this->view->render('user_account_view', ['errors' => $this->changePseudoValidator->getErrors()]);
	}

	public function deleteAccount($account)
	{
		$user = $this->userAuthentication->connectedUser();
		if ($user) {
			if (!empty($account) && $this->deleteAccountValidator->validate($account)) {
				if ($user['email'] == $account['email']) {
					$this->userDAO->deleteAccount($user['id']);
					header('Location: ../public/index.php?route=home');
				}
			}
		}
		$this->view->render('user_account_view', [
			'errors' => $this->deleteAccountValidator->getErrors()
		]);
	}
}

<?php

namespace App\Controllers;

use App\DAO\NewsletterDAO;
use Framework\View;
use App\Models\Visitor;
use Framework\NewsletterValidator;
use Framework\Session;

class NewsletterController
{
	private $newsletterDAO;
	private $view;
    private $visitor;
    private $newsletterValidator;
    private $session;

	public function __construct()
	{
        $this->newsletterDAO = new NewsletterDAO();
        $this->view = new View();
        $this->visitor = new Visitor();
        $this->newsletterValidator = new NewsletterValidator();
        $this->session = new Session();
    }

    public function registerNewsletter($user)
    {
        if (!empty($user) && $this->newsletterValidator->validate($user)) {
    	    $existsEmail = $this->newsletterDAO->existsEmail($user['email']);
            if (!$existsEmail) {
                $this->visitor->create($user['email']);
                $this->newsletterDAO->addVisitor($this->visitor);
                $this->session->setSession('messageNewsletter', 'Vous êtes bien inscris à la neswleter');
                header('Location: ../public/index.php?route=home');
            }
            $this->session->setSession('messageNewsletter',  'L\'email existe déjà, veuillez choisir une autre adresse email');
        }

        $this->view->render('home', ['errors' => $this->newsletterValidator->getErrors()]);
    }
}

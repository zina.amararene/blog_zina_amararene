<?php

namespace App\Controllers;

use Framework\View;
use App\DAO\CommentDAO;
use App\DAO\UserDAO;
use Framework\Email;
use App\DAO\BlackListDAO;
use App\Models\BlackList;
use Framework\PostCommentValidator;
use Framework\RemoveCommentValidator;
use\Framework\UserAuthentication;
use Framework\Session;

class ValidateCommentsController
{
	private $view;
	private $commentDAO;
	private $userDAO;
	private $email;
	private $blackListDAO;
	private $blackList;
	private $postCommentValidator;
	private $userAuthentication;
	private $session;

	public function __construct()
	{
		$this->view = new View();
		$this->commentDAO = new CommentDAO();
		$this->userDAO = new UserDAO();
		$this->email = new Email();
		$this->blackListDAO = new BlackListDAO();
		$this->blackList = new BlackList();
		$this->postCommentValidator = new PostCommentValidator();
		$this->removeCommentValidator = new RemoveCommentValidator();
		$this->userAuthentication = new UserAuthentication();
		$this->session = new Session();
	}

    // Traitement permettant de publier un commentaire après validation
	public function postComment($comment)
	{
		$commentsToValidate = $this->commentDAO->getCommentsToValidate();
        // Si il n'exsite aucun commentaire a valider alors on affiche la page administration
		if (empty($commentsToValidate)) {
			$this->session->setSession('messageAdministration', 'Aucun commentaire a valider');
			header('Location: ../public/index.php?route=administration');
		} elseif (!empty($comment) && !empty($comment['idComment']) && $this->postCommentValidator->validate($comment)) {
			$this->commentDAO->validateComment($comment['idComment']);
			$this->session->setSession('messageCommentValidate', 'Le commentaire est validé et va être publié.');
			header('Location: ../public/index.php?route=validateComments&action=postComment');
		}

		$this->view->render('validate_comments_view', [
			'commentsToValidate' => $commentsToValidate,
			'errors' => $this->postCommentValidator->getErrors()
		]);
	}

	public function warningSendEmailUser($user, $comment, $subject, $file)
	{
		$reporting = $user['reporting'] + 1;
	    // Incrémentation du nombre de commentaire inapproprié dans user
		$this->userDAO->editReporting($user['id'], $reporting);
		// Suppression du commentaire indésirable
		$this->commentDAO->removeComment($comment['idComment']);
        // Un email est envoyé à l'utilisateur quand celui-ci a atteint 2 ou 3 avertissements
        if ($subject != NULL && $file != NULL) {
			$sender = [$user['email']];
		    $body = $this->view->renderEmail($file, []);
		    $this->email->receiveSendEmail($sender, $subject, $body, false, null);
	    }
	}

    //Traitement des commentaires indésirables
	public function removeComment($comment)
	{
		$admin = $this->userAuthentication->connectedUser();
		if ($admin && $admin['role'] == 'administrator') {
			if (!empty($comment) && $this->removeCommentValidator->validate($comment)) {
				$user = $this->userDAO->getUser($comment['pseudo']);
				if ($user) {
					switch ($user['reporting']) {
					case '0':
					    $subject = null;
					    $file = null;
						$this->session->setSession('messageCommentValidate', 'Suppression du commentaire undésirable et signalement du membre en base de données d\'une tentative de publication d\'un commentaire inapproprié');
						break;
					case '1' :
					    // Envoyer un email d\'avertissement au memebre pour cause d'un commentaire indésirable
					    $subject = 'Email de signalement de contenu indésirable de votre commentaire';
					    $file = 'email_user_warning';
					    $this->session->setSession('messageCommentValidate', 'Un email d\'avertissemnt sur des commentaires indésirables à été envoyé au memebre');
					    break;
					case '2' :
					    $subject = 'Vous êtes banni du blog Amararene Zina';
					    $file = 'email_banned_user';
					    if (!$this->blackListDAO->memberIsBlacklisted($user['email'])) {
					    	$this->blackList->create($user['email'], $user['reporting']+1);
					    	 // Ajouter l'email de l'utilisateur dans la blackList et on récupère l'id
						    $idBlackList = $this->blackListDAO->addBlackList($this->blackList);
						    $this->userDAO->banUser($user['id'], $idBlackList);
						    $this->session->setSession('messageCommentValidate', 'Le membre est banni et ajouté dans la liste noire.');
					    }
					    break;
					}
				} 
				$this->warningSendEmailUser($user, $comment, $subject, $file);
				header('Location: ../public/index.php?route=validateComments&action=postComment');
			}
	    }
		$this->view->render('validate_comments_view', ['errors' => $this->removeCommentValidator->getErrors()]);
	}
}
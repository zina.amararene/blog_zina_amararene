<?php

namespace App\Controllers;

use Framework\View;
use Framework\Email;
use App\DAO\UserDAO;
use Framework\Token;
use Framework\ResetPasswordValidator;
use Framework\EditPasswordValidator;
use Framework\Session;

class ForgotPasswordController
{
	private $view;
	private $email;
	private $userDAO;
	private $token;
	private $resetPasswordValidator;
	private $editPasswordValidator;
	private $session;

	public function __construct()
	{
		$this->view = new View();
		$this->email = new Email();
		$this->userDAO = new UserDAO();
		$this->token = new Token();
		$this->resetPasswordValidator = new ResetPasswordValidator();
		$this->editPasswordValidator = new EditPasswordValidator();
		$this->session = new Session();
	}

    // Envoyer à l'utilisateur un email contenant le lien pour réinitialiser le mot de passe
	public function resetPassword($user)
	{
		if (!empty($user) && $this->resetPasswordValidator->validate($user)) {
			$existUser = $this->userDAO->getUser($user['email']);
            if ($existUser) {
            	if ($existUser['reporting'] < 3) {
	            	// Générer un token de sécurité envoyé dans l'email de réinitialisation ainsi que l'id du membre
	            	$tokenResetPassword = $this->token->generateToken('tokenResetPassword');
	            	$this->userDAO->addTokenResetPassword($tokenResetPassword, $existUser['id']);
	            	//Envoyer un email de réinitialisation de mot de passe
	            	$sender = [$user['email']];
					$subject = 'Blog Zina Amararene';
					$body = $this->view->renderEmail('email_reset_password', ['tokenResetPassword' => $tokenResetPassword]);
				    $this->email->receiveSendEmail($sender, $subject, $body, false, null);
					$this->session->setSession('messageForgotPassword', 'Un email vous a été envoyé si votre adresse email existe dans la base de données, cliquez sur le lien pour réinitialiser votre mot de passe');
					header('Location: ../public/index.php?route=forgotPassword&action=resetPassword');
				} 
				if ($existUser['reporting'] >= 3) {
					$this->session->setSession('messageForgotPassword', 'Vous êtes bani du site, vous ne pouvez pas réinitialiser votre mot de passe');	
				}
			} 
			if (!$existUser) {
				$this->session->setSession('messageForgotPassword', 'Email inconnu, vous n\'êts pas inscrit en tant que membre. Vous pouvez vous inscrire via ce formulaire');
		    }
		}

		$this->view->render('forgotPassword_view', [
			'errorsResetPassword' => $this->resetPasswordValidator->getErrorsResetPassword()
		]);
	}

    // Redirection vers un formulaire pour saisir le nouveau mot de passe
    // après vérification du token contenu dans le lien qui permet de réinitialser le mot de passe
	public function verifyTokenResetPassword($token)
	{
		// Vérifier si token transmis par email correspond a celui dans la bdd
		$infoUser = $this->userDAO->verifyTokenRestPassword($token);
		if ($infoUser) {
    		$this->view->render('reset_password_view', ['email' => $infoUser['email']]);
		}
	}

	public function editPassword($resetPassword)
	{
    	if (!empty($resetPassword) && $this->editPasswordValidator->validate($resetPassword)) {
			$newPassword = password_hash($resetPassword['newPassword'], PASSWORD_DEFAULT);
			$infoUser = $this->userDAO->getUser($resetPassword['email']);
			if ($infoUser) {
				$this->userDAO->editPassword($infoUser['id'], $resetPassword['email'], $newPassword);
				$this->userDAO->removeTokenResetPassword($infoUser['id']);
				$this->session->setSession('messageSigIn', 'Vous pouvez vous connecter avec le nouveau mot de passe.');
				header('Location: ../public/index.php?route=myAccount&action=register');
			}
    	}

	    $this->view->render('reset_password_view', [
	    	'errorsEditPassword' => $this->editPasswordValidator->getErrorsEditPassword()
	    ]);
	}
}

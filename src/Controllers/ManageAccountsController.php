<?php

namespace App\Controllers;

use Framework\View;
use App\DAO\UserDAO;
use\Framework\UserAuthentication;
use\Framework\CreateRoleValidator;

class ManageAccountsController
{
	private $view;
	private $userDAO;
	private $userAuthentication;
	private $createRoleValidator;

	public function __construct()
	{
		$this->view = new View();
		$this->userDAO = new UserDAO();
		$this->userAuthentication = new UserAuthentication();
		$this->createRoleValidator = new CreateRoleValidator();
    }

    public function listOfAccounts()
    {
        $accounts = $this->userDAO->getUsers();
        $this->view->render('list_accounts_view', ['accounts' => $accounts]);
	}
	
	public function createRole($role, $userId)
	{
		$authenticatedUser = $this->userAuthentication->connectedUser();
		if ($authenticatedUser && $authenticatedUser['role'] == 'administrator') {
			$accounts = $this->userDAO->getUsers();
			if (!empty($role) && !empty($userId) && $this->createRoleValidator->validate($role, $userId)) {
				$user = $this->userDAO->userExists($userId);
				$this->userDAO->createRole($role['role'], $userId);
			}
	    }
		$this->view->render('list_accounts_view', [
			'accounts' => $accounts,
			'errors' => $this->createRoleValidator->getErrors()
		]);
	}
}

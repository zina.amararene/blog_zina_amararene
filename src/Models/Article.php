<?php

namespace App\Models;

class Article
{
    private $id;
    private $title;
    private $author;
    private $abstract;
    private $content;
    private $dateAdded;
    private $dateModification;
    private $userId;

    public function create($title, $author, $abstract, $content, $userId)
    {
        $this->title = $title;
        $this->author = $author;
        $this->abstract = $abstract;
        $this->content = $content;
        $this->dateAdded = new \DateTimeImmutable();
        $this->dateModification = null;
        $this->userId = $userId;
    }

    public function update($title, $author, $abstract, $content) {
        $this->title = $title;
        $this->author = $author;
        $this->abstract = $abstract;
        $this->content = $content;
        $this->dateModification = new \DateTime();
    }

    public function hydrate(array $article)
    {
        $this->id = $article['id'];
        $this->title = $article['title'];
        $this->author = $article['author'];
        $this->abstract = $article['abstract'];
        $this->content = $article['content'];
        $this->dateAdded = \DateTime::createFromFormat('Y-m-d H:i:s', $article['date_added']);
        $this->dateModification = \DateTime::createFromFormat('Y-m-d H:i:s', $article['date_modification']);
        $this->userId = $article['user_id'];
    }

    // Les getters
    public function getId()
    {
    	return $this->id;
    }

    public function getTitle()
    {
    	return $this->title;
    }

    public function getAuthor()
    {
    	return $this->author;
    }

    public function getAbstract()
    {
        return $this->abstract;
    }

    public function getContent()
    {
    	return $this->content;
    }

    public function getDateAdded()
    {
    	return $this->dateAdded;
    }

    public function getDateModification()
    {
    	return $this->dateModification;
    }

    public function getUserId()
    {
    	return $this->userId;
    }

    // Les setters
    public function setDateAdded(\DateTime $dateAdded)
    {
        $this->dateAdded =  $dateAdded;
    }

    public function setUpDatedate(\DateTime $dateModification)
    {
        $this->dateModification = $dateModification;
    }

    public function setId($id)
    {
    	$id = (int) $id;
    	if ($id > 0) {
    		$this->id = $id;
    	}
    }

    public function setTitle($title)
    {
    	if (strlen($title) > 0 && strlen($title) <= 100) {
    		$this->title = $title;
    	}
    }

    public function setAuthor($author)
    {
    	if (is_string($author) && strlen($author) > 0 && strlen($author) <= 100) {
    		$this->author = $author;
    	}
    }

    public function setAbstract($abstract)
    {
        if (strlen($abstract) >= 100 && strlen($abstract) <= 800) {
            $this->abstract = $abstract ;
        }
    }

    public function setContent($content)
    {
    	if (strlen($content) >= 2000 && strlen($content) <= 7000) {
    		$this->content = $content ;
    	}
    }

    public function setDateModification(\DateTime $dateModification)
    {
    	$this->dateModification = $dateModification;
    }

    public function setUserId($userId)
    {
    	$userId = (int) $userId;
    	if ($userId > 0) {
            $this->userId = $userId;
    	}
    }
}

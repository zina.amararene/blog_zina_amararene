<?php

namespace App\Models;

class BlackList
{
	private $id;
    private $emailUser;
	private $numberWarning;

    public function create($email, $number)
    {
        $this->emailUser = $email;
        $this->numberWarning = $number;
    }

    // Les getters
	public function getId()
	{
		return $this->id;
	}

    public function getEmailUser()
    {
        return $this->emailUser;
    }

	public function getNumberWarning()
	{
		return $this->numberWarning;
	}

	// Les setters

    public function setId($id)
    {
    	$id = (int) $id;
    	if ($id > 0) {
    		$this->id = $id;
    	}
    }

    public function setEmailUser($email)
    {
        if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email)) {
            $this->emailUser = $email;
        }
    }

    public function setNumberWarning($numberWarning)
    {
    	if (is_numeric($numberWarning)) {
    		$this->numberWarning = $numberWarning;
    	}
    }
}

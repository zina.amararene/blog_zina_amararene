<?php

namespace App\Models;

class Newsletter
{
	private $id;
	private $content;
	private $dateCretaion;

    public function create($content)
    {
        $this->content = $content;
        $this->dateCreation = new \DateTimeImmutable();
    }

	public function update($content)
    {
        $this->content = $content;
    }

    // Les getters
	public function getId()
	{
		return $this->id;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function getDateCretaion()
	{
		return $this->dateCretaion;
	}

	// Les setters
	public function setId($id)
    {
    	$id = (int) $id;
    	if ($id > 0) {
    		$this->id = $id;
    	}
    }

    public function setContent($content)
    {
    	if (strlen($content) > 0) {
    		$this->content = $content ;
    	}
    }

    public function setDateCretaion($dateCretaion)
    {
    	$this->dateCretaion = $dateCretaion;
    }
}

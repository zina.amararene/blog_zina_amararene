<?php

namespace App\Models;

class Picture
{
	private $id;
	private $name;
	private $imageSize;
	private $imageType;
	private $articleId;

    public function create(array $picture, $articleId)
    {
		$this->name = $picture['name'];
		$this->imageSize = $picture['imageSize'];
		$this->imageType = $picture['imageType'];
    	$this->articleId = $articleId;
    }

    public function hydrate(array $picture)
    {
    	$this->id = $picture['id'];
    	$this->name = $picture['name'];
    	$this->imageSize = $picture['image_size'];
    	$this->imageType = $picture['image_type'];
    	$this->articleId = $picture['article_id'];
    }

	// Les getters
	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getImageSize()
	{
		return $this->imageSize;
	}

	public function getImageType()
	{
		return $this->imageType;
	}

	public function getArticleId()
	{
		return $this->articleId;
	}

	//Les setters
	public function setId()
	{
		$id = (int) $id;
    	if ($id > 0) {
    		$this->id = $id;
    	}
	}

	public function setName($name)
	{
		if (strlen($name) > 0 && strlen($name) <= 100) {
    		$this->name = $name;
    	}
	}

	public function setImageSize($imageSize)
	{
		if (is_numeric($imageSize)) {
			$this->imageSize = $imageSize;
		}
	}

	public function setImageType($imageType)
	{
		if(strlen($imageType) > 0 && strlen($imageType) <= 30) {
			$this->imageType = $imageType;
		}
	}

	public function setArticleId($articleId)
	{
		$this->articleId = $articleId;
	}
}

<?php

namespace App\Models;

class Visitor
{
	private $id;
	private $email;
    private $unsubscribe;

    public function create($email)
    {
        $this->email = $email;
        $this->unsubscribe = false;
    }

    // Les getters
	public function getId()
	{
		return $this->id;
	}

	public function getEmail()
	{
		return $this->email;
	}

    public function getUnsubscribe()
    {
        return $this->unsubscribe;
    }

	// Les setters
	public function setId($id)
    {
    	$id = (int) $id;
    	if ($id > 0) {
    		$this->id = $id;
    	}
    }

    public function setEmail($email)
    {
    	if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email)) {
    	    $this->email = $email;
    	}
    }

    public function setUnsubscribe($unsubscribe)
    {
        $this->unsubscribe = $unsubscribe;
    }
}

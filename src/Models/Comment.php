<?php

namespace App\Models;

class Comment
{
	private $id;
	private $pseudo;
	private $content;
    private $toPublish;
	private $undesirable;
	private $dateAdded;
	private $articleId;

    public function create($pseudo, $content, $articleId)
    {
        $this->pseudo = $pseudo;
        $this->content = $content;
        $this->toPublish = false;
        $this->undesirable = false;
        $this->dateAdded = new \DateTimeImmutable();
        $this->articleId = $articleId;
    }

    public function update($pseudo, $content, $toPublish, $articleId) {
        $this->pseudo = $pseudo;
        $this->content = $content;
        $this->toPublish = $toPublish;
    }

    public function hydrate(array $comment)
    {
        $this->id = $comment['id'];
        $this->pseudo = $comment['pseudo'];
        $this->content = $comment['content'];
        $this->toPublish = $comment['to_publish'];
        $this->undesirable = $comment['undesirable'];
        $this->dateAdded = \DateTime::createFromFormat('Y-m-d H:i:s', $comment['date_added']);
        $this->articleId = $comment['article_id'];
    }

	// Les getters
	public function getId()
	{
		return $this->id;
	}

	public function getPseudo()
	{
		return $this->pseudo;
	}

	public function getContent()
	{
		return $this->content;
	}

    public function getToPublish()
    {
        return $this->toPublish;
    }

    public function getUndesirable()
    {
    	return $this->undesirable;
    }

    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    public function getArticleId()
    {
    	return $this->articleId;
    }

    // Les setters
    public function setId($id)
    {
    	$id = (int) $id;
    	if ($id > 0) {
    		$this->id = $id;
    	}
    }

    public function setPseudo($pseudo)
    {
    	if (is_numeric($pseudo)) {
    		throw new \Exception('Le pseudo ne doit pas être un nombre.');
    	} elseif (strlen($pseudo) > 0 && strlen($pseudo) <= 50) {
    		$this->pseudo = $pseudo;
    	}
    }

    public function setContent($content)
    {
    	if (strlen($content) >= 5 && strlen($content) <= 1000) {
            $this->content = $content;
    	}
    }

    public function setToPublish($toPublish)
    {
        if (is_bool($toPublish)) {
            $this->toPublish = $toPublish;
        }
    }

    public function setUndesirable($undesirable)
    {
    	if (is_bool($undesirable)) {
    	    $this->undesirable = $undesirable;
    	}
    }

    public function setDateAdded(\DateTime $dateAdded)
    {
        $this->dateAdded = $dateAdded;
    }

    public function setArticleId($articleId)
    {
    	$this->articleId = $articleId;
    }
}

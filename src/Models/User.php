<?php

namespace App\Models;

class User
{
	private $id;
	private $lastName;
	private $firstName;
	private $pseudo;
	private $email;
	private $password;
	private $resetPassword;
	private $role;
	private $superAdmin;
	private $reporting;
	private $dateConnection;
	private $blackListId;
	private $newsletterId;

	public function create($lastName, $firstName, $pseudo, $email, $password)
	{
		$this->lastName = $lastName;
		$this->firstName = $firstName;
		$this->pseudo = $pseudo;
		$this->email = $email;
		$this->password = $password;
		$this->resetPassword = NULL;
		$this->role = 'member';
		$this->superAdmin = false;
		$this->reporting = 0;
		$this->dateConnection = new \DateTimeImmutable();
		$this->blackListId = NULL;
		$this->newsletterId = NULL;
	}

	public function hydrate(array $user)
    {
		$this->id = $user['id'];
		$this->lastName = $user['last_name'];
		$this->firstName = $user['first_name'];
		$this->pseudo = $user['pseudo'];
		$this->email = $user['email'];
		$this->password = $user['password'];
		$this->resetPassword = $user['reset_password'];
		$this->role = $user['role'];
		$this->superAdmin = $user['super_admin'];
		$this->reporting = $user['reporting'];
		$this->dateConnection = $user['date_connection'];
		$this->blackListId = $user['blackList_id'];
		$this->newsletterId = $user['newsletter_id'];
	}

	public function update($lastName, $firstName, $pseudo, $email, $password, $blackListId, $newsletterId)
	{
		$this->lastName = $lastName;
		$this->firstName = $firstName;
		$this->pseudo = $pseudo;
		$this->email = $email;
		$this->password = $password;
		$this->blackListId = $blackListId;
		$this->newsletterId = $newsletterId;
	}

	//Les getters
	public function getId()
	{
		return $this->id;
	}

	public function getLastName()
	{
		return $this->lastName;
	}

	public function getFirstName()
	{
		return $this->firstName;
	}

	public function getPseudo()
	{
		return $this->pseudo;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function getResetPassword()
	{
		return $this->resetPassword;
	}

	public function getRole()
	{
		return $this->role;
	}

	public function getSuperAdmin()
	{
		return $this->superAdmin;
	}

	public function getReporting()
	{
		return $this->reporting;
	}

	public function getDateConnection()
	{
		return $this->dateConnection;
	}

	public function getBlackListId()
	{
		return $this->blackListId;
	}

	public function getNewsletterId()
	{
		return $this->newsletterId;
	}

	//Les setters
	public function setId()
	{
		$id = (int) $id;
    	if (!empty($id) && $id > 0) {
    		$this->id = $id;
    	}
	}

	public function setLastName($lastName)
	{
		if (strlen($lastName) > 0 && strlen($lastName) <= 100) {
    		$this->lastName = $lastName;
    	}
	}

	public function setFirstName($firstName)
	{
		if (strlen($firstName) > 0 && strlen($firstName) <= 100) {
    		$this->firstName = $firstName;
    	}
	}


	public function setPseudo($pseudo)
	{
		if (strlen($pseudo) >= 6 && strlen($pseudo) <= 50) {
    		$this->pseudo = $pseudo;
    	}
	}

	public function setEmail($email)
    {
    	if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email)) {
    	    $this->email = $email;
    	}
    }

	public function setPassword($password)
	{
		$this->password = $password;
	}

	public function setResetPassword($resetPassword)
	{
		$this->resetPassword = $resetPassword;
	}

	public function setRole($role)
	{
	    $this->role = $role;
	}

	public function setSuperAdmin($superAdmin)
	{
		if (is_bool($superAdmin)) {
    	    $this->superAdmin = $superAdmin;
    	}
	}

	public function setReporting($reporting)
	{
		$reporting = (int) $reporting;
		if (is_numeric($reporting)) {
		    $this->reporting = $reporting;
		}
	}

	public function setDateConnection($dateConnection)
	{
		$this->dateConnection = $dateConnection;
	}

	public function setBlackListId($blackListId)
	{
		$this->blackListId = $blackListId;
	}

	public function setNewsletterId($newsletterId)
	{
		$this->newsletterId = $newsletterId;
	}
}
